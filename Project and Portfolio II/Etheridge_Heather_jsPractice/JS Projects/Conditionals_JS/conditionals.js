 //Problem #1 Temperature Converter

            //First ask the user for a temerature 
            var tempString = prompt("Hello, this program will convert temperature from Celcius to Farenheit, and from Farenheit to Celcius.\r\nPlease enter a temperature:");
            
            //Test to make sure user entered something
            while (tempString === null || isNaN(parseFloat(tempString)) || tempString.trim() === "") {
                if(tempString === null || tempString.trim() === ""){
                    tempString = prompt("Please do not leave this blank. Enter a temperature as a number:");
                } else {
                    tempString = prompt("Oops, you entered something other than a number.\r\nPlease enter a temperature as a number:");
                }
            }

            //Convert string to correct data type
            var tempNum = parseFloat(tempString);
            console.log(tempNum);

            /* 
            Test to make sure number was converted properly
            tempNum = tempNum * 2;
            Console.WriteLine(tempNum);
            */

            //Now ask the user if the temperature entered is in Celcius or Farenheit
            var tempTypeString = prompt("If the temperature you entered was measured in Celcius enter C, if was measured in Farenheit, enter F:");
 
            //Test to see if user entered C or F
            while (tempTypeString.toLowerCase() != "c" && tempTypeString.toLowerCase() != "f")
            {
                tempTypeString = prompt("Oops, you've entered something other than C or F.\r\nPlease enter C or F: ");
            }
            //formulas for each conversion
            var toCelcius = (((tempNum - 32) * 5) / 9);
            var toFarenheit = (((tempNum * 9) / 5) + 32);

            //Statement to calculate the conversion
            if (tempTypeString == 'c') {
                alert("The temperature is " + toFarenheit + " degrees Farenheit.");
            } else {
                alert("The temperature is "+ toCelcius + " degrees Celcius.");
            }

            /*Test Values
            tempNum = 32; tempType = 'F';  //Returns 0 Celcius
            tempNum = 100; tempType = 'C'; //Returns 212 Farenheit
            tempType = 'c'; //Does make a difference, returs errors
            tempNum = 50; tempType = 'F'; //Returns 10 degrees Celcius*/




            //Problem #2

            //Ask the user how many gallons their car's tank can hold
            tankString = prompt("\r\nWe will determine if you need to get gas now to make it accross the dessert.\r\nHow many gallons of gas can your car's tank hold?");
            //Test to see if the user input correct data type
            while (tankString === null || isNaN(parseFloat(tankString)) || tankString.trim() === ""){
                if(tankString === null || tankString.trim() === ""){
                    tankString = prompt("Please do not leave this blank. \r\nEnter the number of gallons of gas that your car's tank can hold:");
                } else {
                    tankString = prompt("Oops, you entered something other than a number.\r\nPlease enter the number of gallons of gas that your car's tank can hold:");
                }
            }
            
            //Convert string into useable datatype 
            var tankNum = parseFloat(tankString);

            //Ask the user how full their gas tank is
            var percentString = prompt ("What percentage of fullness is your gas tank currently at?");

            //Test to see if the usesr input correct data type
            while (percentString === null || isNaN(parseFloat(percentString)) || percentString.trim() === ""){
                if(percentString === null || percentString.trim() === ""){
                    percentString = prompt("Please do not leave this blank. \r\nWhat percentage of fullness is your gas tank currently at?");
                } else {
                    percentString = prompt("Oops, you entered something other than a number.\r\nPlease enter the percentage of fullness your gas tank is currently at.\r\nEnter as a number 1-100:");
                }
            }

            //Test to see if percentString is a number between 1-100
            while (parseFloat(percentString) < 1 || parseFloat(percentString) > 100){
                percentString = prompt("Only enter numbers 1-100.\r\nWhat percentage of fullness is your gas tank currently at?");
            }

            //Convert sting into useable datatype
            var percentNum = parseFloat(percentString);

            

            //Ask the user how many miles per gallon does their car go
            var mpgString = prompt("How many miles per gallon does your car get?");

            //Test to see if the user input correct data type
            while (mpgString === null || isNaN(parseFloat(mpgString)) || mpgString.trim() === ""){
                if(mpgString === null || mpgString.trim() === ""){
                    mpgString = prompt("Please do not leave this blank. \r\nHow many miles per gallon does your car get?");
                } else {
                    mpgString = prompt("Oops, you entered something other than a number.\r\nPlease enter the number of miles per gallon your car gets:");
                }
            }
            //Convert string into useable datatype
            var mpgNum = parseFloat(mpgString);

            //Create variable that will find how many gallons in tank
            var gallonsInTank = (tankNum * percentNum) / 100;
            //Test line to see if math worked
            //Console.WriteLine(milesInTank); 
            //Create a variable to find how many miles the car can go before stoping
            var howFar = gallonsInTank * mpgNum;
            //Output to the user whether or not they can make the trip
            if (howFar >= 200){
                alert("Yes, you can drive " + howFar + " more miles, and you can make it without stopping for gas!");
            }
            else
            {
                alert("You only have " + gallonsInTank + " gallons of gas in your tank, better get gas now while you can!");
            }

            /*
            Test Values
            Gallons 20 Tank 50% Mpg 25
            Yes, you can drive 250 more miles, and you can make it without stopping for gas!

            Gallons 12 Tank 60% Mpg 20
            You only have 7.2 gallons of gas in your tank, better get gas now while you can!

            Gallons 50 Tank 30% Mpg 15
            Yes, you can drive 225 more miles, and you can make it without stopping for gas!                                        
            */

          
            //Problem 3 Grade Letter Calculator

            //Let the user know what the program does and promt them
            var gradeString = prompt("Hello, this program will convert your perctntage grade into a letter grade based on Full Sail's grading scale.\r\nPlease enter your current course grade:");

            //Test to see if user entered a number
            while (gradeString === null || isNaN(parseFloat(gradeString)) || gradeString.trim() === ""){
                if(gradeString === null || gradeString.trim() === ""){
                    gradeString = prompt("Please do not leave this blank. \r\nPlease enter your current course grade:");
                } else {
                    gradeString = prompt("Oops, you entered something other than a number.\r\nPlease enter your current course grade as a number:");
                }
            }

            //Convert string into useable number
            var gradeNum = parseFloat(gradeString);

            //Start our conditional to convert the grade to a letter grade
            if (gradeNum <= 69){
               alert("You have a " + gradeNum +" which means you have earned a F in the class!");
            } else if (gradeNum <= 72) {
                alert("You have a " + gradeNum +" which means you have earned a D in the class!");
            } else if (gradeNum <= 79) {
                alert("You have a " + gradeNum +" which means you have earned a C in the class!");
            } else if (gradeNum <= 89) {
                alert("You have a " + gradeNum +" which means you have earned a B in the class!");
            } else if (gradeNum <= 100) {
                alert("You have a " + gradeNum +" which means you have earned a A in the class!");
            }

            /*
            Test Values
            Grade 92
            You have a 92%, which means you have earned an A in the class!
            Grade 80
            You have a 80%, which means you have earned a B in the class!
            Grade 67
            You have a 67%, which means you have earned a F in the class!
            Grade 120
            You must enter your grade as a number between 1 and 100.  What is your current grade?
            Grade 72
            You have a 72%, which means you have earned a D in the class!
            */

            //Problem 4 Discount double check

            //Let the user know what the program does and prompt for price of first item
            var firstPriceString = prompt("Let's see how much the total of your purchase is minus any discounts!\r\nEnter the price of your first item:");

            //Validate input
            while (firstPriceString === null || isNaN(parseFloat(firstPriceString)) || firstPriceString.trim() === ""){
                if(firstPriceString === null || firstPriceString.trim() === ""){
                    firstPriceString = prompt("Please do not leave this blank. \r\nEnter the price of your first item:");
                } else {
                    firstPriceString = prompt("Oops, you entered something other than a number.\r\nPlease enter the price of your first item as a number:");
                }
            }
         
            //Convert price of first item into a number
            var firstPriceNum = parseFloat(firstPriceString);

            //Ask for price of second item
            var secondPriceString = prompt("Enter the price of your second item:");

            //Validate
                      while (secondPriceString === null || isNaN(parseFloat(secondPriceString)) || secondPriceString.trim() === ""){
                if(secondPriceString === null || secondPriceString.trim() === ""){
                    secondPriceString = prompt("Please do not leave this blank. \r\nEnter the price of your second item:");
                } else {
                    secondPriceString = prompt("Oops, you entered something other than a number.\r\nPlease enter the price of your second item as a number:");
                }
            }

            //Convert price of second item into a number
            var secondPriceNum = parseFloat(secondPriceString);

            //Calculate total
            var subTotal = firstPriceNum + secondPriceNum;
            var grandTotal;
            //Use a conditional to calculate total incuding discount if any
            if (subTotal >= 100){
                grandTotal = subTotal - (subTotal * .10);
                alert("Your total purchase is $" + grandTotal + " which includes your 10% discount.");
            } else if (subTotal < 100 && subTotal >= 50){
                grandTotal = subTotal - (subTotal * .05);
                alert("Your total purchase is $" + grandTotal + " which includes your 5% discount.");
            } else if (subTotal < 50) {
                alert("Your total purchase is $" + subTotal);
            }


            /*
            Test Values

            1st item 45.50  2nd item 75  total 108.50
            Your total purchase is $108.45, which includes your 10% discount.

            1st item 30 2nd item 25 total 52.25
            Your total purchase is $52.25, which includes your 5% discount.

            1st item 5.75  2nd item 12.50 total 18.25
            Your total purchase is $18.25.

            1st item 200 2nd item 5 total
            Your total purchase is $184.5, which includes your 10% discount.
           */