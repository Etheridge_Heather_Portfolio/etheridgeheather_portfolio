﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etheridge_Heather_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            //  NAME:  Heather Etheridge
            //  DATE:  January 15, 2017
            //  Section #02
            //  Scalable Data Infrastructures
            //  Conditionals


            //Problem #1 Temperature Converter

            //First ask the user for a temerature 
            Console.WriteLine("Hello, this program will convert temperature from Celcius to Farenheit, and from Farenheit to Celcius.\r\nPlease enter a temperature:");
            string tempString = Console.ReadLine();
            //Test to make sure user entered something
            double tempInt;
            while (string.IsNullOrWhiteSpace(tempString))
            {
                Console.WriteLine("Please do not leave this blank.  Enter a temperature:");
                tempString = Console.ReadLine();
                string.IsNullOrWhiteSpace(tempString);
            }
            while (!(double.TryParse(tempString, out tempInt)))
            {
                Console.WriteLine("Oops, you entered something other than a number.\r\nPlease enter a temperature as a number:");
                tempString = Console.ReadLine();
                double.TryParse(tempString, out tempInt);
            }

            //Convert string to correct data type
            double tempNum = double.Parse(tempString);

            /* 
            Test to make sure number was converted properly
            tempNum = tempNum * 2;
            Console.WriteLine(tempNum);
            */

            //Now ask the user if the temperature entered is in Celcius or Farenheit

            Console.WriteLine("If the temperature you entered was measured in Celcius enter C, if was measured in Farenheit, enter F:");
            string tempTypeString = Console.ReadLine();

            //Test to see if user entered C or F
            while (tempTypeString != "C" && tempTypeString != "F" && tempTypeString != "c" && tempTypeString != "f")
            {
                Console.WriteLine("Oops, you've entered something other than C or F.\r\nPlease enter C or F: ");
                tempTypeString = Console.ReadLine();

            }
            char tempType = char.Parse(tempTypeString);
            //formulas for each conversion
            double toCelcius = (((tempNum - 32) * 5) / 9);
            double toFarenheit = (((tempNum * 9) / 5) + 32);
            //Statement to calculate the conversion

            if (tempType == 'C')
            {
                Console.WriteLine("The temperature is {0} degrees Farenheit.", toFarenheit);
            }
            else if (tempType == 'c')
            {
                Console.WriteLine("The temperature is {0} degrees Farenheit.", toFarenheit);
            }
            else
            {
                Console.WriteLine("The temperature is {0} degrees Celcius.", toCelcius);
            }

            /*Test Values
            tempNum = 32; tempType = 'F';  //Returns 0 Celcius
            tempNum = 100; tempType = 'C'; //Returns 212 Farenheit
            tempType = 'c'; //Does make a difference, returs errors
            tempNum = 50; tempType = 'F'; //Returns 10 degrees Celcius*/




            //Problem #2

            //Ask the user how many gallons their car's tank can hold
            Console.WriteLine("\r\nWe will determine if you need to get gas now to make it accross the dessert.\r\nHow many gallons of gas can your car's tank hold?");
            string tankString = Console.ReadLine();
            //Test to see if the user input correct data type
            double tankDouble;
            while (string.IsNullOrWhiteSpace(tankString) || !(double.TryParse(tankString, out tankDouble)))
            {
                if (string.IsNullOrWhiteSpace(tankString))
                {
                    Console.WriteLine("Please do not leave blank.  Enter the number of gallons of gas your car's tank can hold:");
                }
                else if (!(double.TryParse(tankString, out tankDouble)))
                {
                    Console.WriteLine("You have entered something other than a number.\r\nPlease enter the number of gallons of gas your car's tank can hold as a number:");
                }

                tankString = Console.ReadLine();
            }



            //Convert string into useable datatype 
            double tankNum = double.Parse(tankString);

            //Ask the user how full their gas tank is
            Console.WriteLine("What percentage of fullness is your gas tank currently at?");
            string percentString = Console.ReadLine();


            //Test to see if the usesr input correct data type
            double percentDouble;
            while (!(double.TryParse(percentString, out percentDouble)))
            {
                Console.WriteLine("You must enter your gas tank fullness percentage as a number 1-100:");
                percentString = Console.ReadLine();
                double.TryParse(percentString, out percentDouble);
            }

            //Convert sting into useable datatype
            double percentNum = double.Parse(percentString);

            //Ask the user how many miles per gallon does their car go
            Console.WriteLine("How many miles per gallon does your car get?");
            string mpgString = Console.ReadLine();

            //Test to see if the user input correct data type
            double mpgDouble;
            while (!(double.TryParse(mpgString, out mpgDouble)))
            {
                Console.WriteLine("You must enter the miles per gallon that your car gets as a number:");
                mpgString = Console.ReadLine();
                double.TryParse(mpgString, out mpgDouble);
            }

            //Convert string into useable datatype
            double mpgNum = double.Parse(mpgString);

            //Create variable that will find how many gallons in tank
            double gallonsInTank = (tankNum * percentNum) / 100;
            //Test line to see if math worked
            //Console.WriteLine(milesInTank); 
            //Create a variable to find how many miles the car can go before stoping
            double howFar = gallonsInTank * mpgDouble;
            //Output to the user whether or not they can make the trip
            if (howFar >= 200)
            {
                Console.WriteLine("Yes, you can drive {0} more miles, and you can make it without stopping for gas!", howFar);
            }
            else
            {
                Console.WriteLine("You only have {0} gallons of gas in your tank, better get gas now while you can!", gallonsInTank);
            }

            /*
            Test Values
            Gallons 20 Tank 50% Mpg 25
            Yes, you can drive 250 more miles, and you can make it without stopping for gas!

            Gallons 12 Tank 60% Mpg 20
            You only have 7.2 gallons of gas in your tank, better get gas now while you can!

            Gallons 50 Tank 30% Mpg 15
            Yes, you can drive 225 more miles, and you can make it without stopping for gas!                                        
            */



            //Problem 3 Grade Letter Calculator

            //Let the user know what the program does and promt them
            Console.WriteLine("\r\nHello, this program will convert your perctntage grade into a letter grade based on Full Sails grading scale.\r\nPlease enter your current course grade.");
            //Create a variable to hold the users response
            string numberGradeString = Console.ReadLine();
            //Test to see if user entered a number
            double numberGrade;
            while (!double.TryParse(numberGradeString, out numberGrade) || numberGrade > 100)
            {
                Console.WriteLine("You must enter your grade as a number between 1 and 100.  What is your current grade?");
                numberGradeString = Console.ReadLine();
                double.TryParse(numberGradeString, out numberGrade);
            }



            //Start our conditional to convert the grade to a letter grade
            if (numberGrade <= 69)
            {
                Console.WriteLine("You have a {0}%, which means you have earned a F in the class!", numberGrade);
            }
            else if (numberGrade <= 72)
            {
                Console.WriteLine("You have a {0}%, which means you have earned a D in the class!", numberGrade);
            }
            else if (numberGrade <= 79)
            {
                Console.WriteLine("You have a {0}%, which means you have earned a C in the class!", numberGrade);
            }
            else if (numberGrade <= 89)
            {
                Console.WriteLine("You have a {0}%, which means you have earned a B in the class!", numberGrade);
            }
            else if (numberGrade <= 100)
            {
                Console.WriteLine("You have a {0}%, which means you have earned an A in the class!", numberGrade);
            }

            /*
            Test Values
            Grade 92
            You have a 92%, which means you have earned an A in the class!
            Grade 80
            You have a 80%, which means you have earned a B in the class!
            Grade 67
            You have a 67%, which means you have earned a F in the class!
            Grade 120
            You must enter your grade as a number between 1 and 100.  What is your current grade?
            Grade 72
            You have a 72%, which means you have earned a D in the class!
            */

            //Problem 4 Discount double check

            //Let the user know what the program does and prompt for price of first item
            Console.WriteLine("\r\nLet's see how much the total of your purchase is minus any discounts!\r\nEnter the price of your first item.");
            string firstPriceString = Console.ReadLine();
            //Validate input
            double firstPrice;
            while (!double.TryParse(firstPriceString, out firstPrice))
            {
                Console.WriteLine("Please only enter numbers.  What is the price of your first item?");
                firstPriceString = Console.ReadLine();
                double.TryParse(firstPriceString, out firstPrice);
            }
            //Ask for price of second item
            Console.WriteLine("Enter the price of your second item.");
            string secondPriceString = Console.ReadLine();
            //Validate
            double secondPrice;
            while (!double.TryParse(secondPriceString, out secondPrice))
            {
                Console.WriteLine("Please only enter numbers.  What is the price of your second item?");
                secondPriceString = Console.ReadLine();
                double.TryParse(secondPriceString, out secondPrice);
            }
            //Create a variable to hold the price of both items
            double subTotal = firstPrice + secondPrice;
            double grandTotal;
            //Use a conditional to calculate total incuding discount if any
            if (subTotal >= 100)
            {
                grandTotal = subTotal - (subTotal * .10);
                Console.WriteLine("Your total purchase is ${0}, which includes your 10% discount.", grandTotal);
            }
            else if (subTotal < 100 && subTotal >= 50)
            {
                grandTotal = subTotal - (subTotal * .05);
                Console.WriteLine("Your total purchase is ${0}, which includes your 5% discount.", grandTotal);
            }
            else if (subTotal < 50)
            {
                Console.WriteLine("Your total purchase is ${0}.", subTotal); ;
            }


            /*
            Test Values

            1st item 45.50  2nd item 75  total 108.50
            Your total purchase is $108.45, which includes your 10% discount.

            1st item 30 2nd item 25 total 52.25
            Your total purchase is $52.25, which includes your 5% discount.

            1st item 5.75  2nd item 12.50 total 18.25
            Your total purchase is $18.25.

            1st item 200 2nd item 5 total
            Your total purchase is $184.5, which includes your 10% discount.
           */
        }
    }
}
