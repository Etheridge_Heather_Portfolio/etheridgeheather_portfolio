﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etheridge_Heather_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Heather Etheridge
            January 22, 2017
            Section 02
            Assignment: Methods
            */



            //Problem #1 Painting A Wall

            //Variables            
            decimal width;
            decimal height;
            int numberCoats;
            decimal paintCovers;

            //Introduce the program and ask for width of wall in ft
            Console.WriteLine("Hello! This program will find out how many gallons of paint it will take to paint a wall.\r\nWhat is the width, in feet, of the wall you would like to paint?");
            //create a variable to hold response
            string widthString = Console.ReadLine();
            //validate user response using while loop
            while (!decimal.TryParse(widthString, out width)) {
                Console.WriteLine("Please only enter numbers and do not leave blank.\r\nWhat is the width, in feet, of the wall you would like to paint?");
                //recapture response
                widthString = Console.ReadLine();
            }
            //Ask for the height of the wall
            Console.WriteLine("In feet, what is the height of the wall you would like to paint?");
            //Create a variable to hold the response
            string heightString = Console.ReadLine();
            //validate user resonse using while loop
            while (!decimal.TryParse(heightString, out height)) {
                Console.WriteLine("Please only enter numbers and do not leave blank.\r\nIn feet, what is the height of the wall you would like to paint?");
                heightString = Console.ReadLine();
            }
            //Ask for the number of coats of paint they would like to use
            Console.WriteLine("How many coats would you like to paint?");
            string numberCoatsString = Console.ReadLine();
            //Validate user response
            while (!int.TryParse(numberCoatsString, out numberCoats)) {
                Console.WriteLine("Please only enter whole numbers and do not leave blank.\r\nHow many coats would you like to paint?");
                numberCoatsString = Console.ReadLine();
            }
            //Ask for the surface area one gallon of paint will cover in feet squared
            Console.WriteLine("What is the surface area one gallon of your paint can cover in square feet?");
            string paintCoversString = Console.ReadLine();
            //Validate response
            while (!decimal.TryParse(paintCoversString, out paintCovers)) {
                Console.WriteLine("Please only enter numbers and do not leave blank.\r\nWhat is the surface area one gallon of your paint can cover in square feet?");
                paintCoversString = Console.ReadLine();
            }
            //Call the method to calculate the number of gallons needed
            decimal totalGallons = GallonsNeeded(width, height, paintCovers, numberCoats);
            Console.WriteLine("For {0} coats on that wall, you will need {1} gallons of paint.", numberCoats, Math.Round(totalGallons,2));

            /*
            Test Values

            width-8 height-10 coats-2 SurfaceArea-300
            = For 2 coats on that wall you will need.53 gallons of paint

            width-30 height-12.5 coats-3 surface area-350
            = For 3 coats on that wall you will need 3.21 gallons of paint

            width-15 height 17.5 coats 4 surface area-250
            = For 4 coats on that wall you will need 4.20 gallons of paint                     
            */
            Console.WriteLine("\r\n");




            //Problem # 2 Stung!

            //Variables
            decimal weight;

            //Introduce the program and ask the animals weight
            Console.WriteLine("We are going to calculate how many bee stings it would take to kill an animal.\r\nWhat is the weight of the animal in pounds?");
            string weightString = Console.ReadLine();
            //Validate
            while (!decimal.TryParse(weightString, out weight)) {
                Console.WriteLine("Please only enter numbers and do not leave blank.\r\nWhat is the weight of the animal in pounds?");
                weightString = Console.ReadLine();
            }
            //Call the method to calculate response
            decimal stings = NumberOfStings(weight);
            Console.WriteLine("It takes {0} bee stings to kill this animal.",stings);

            /*
            Test Values

            weight 10 =
            It takes 90 bee stings to kill this animal.

            weight 160 =
            It takes 1440 bee stings to kill this animal.

            weight 350 = 
            It takes 3150 bee stings to kill this animal.            
            */
            Console.WriteLine("\r\n");


            //Problem # 3 Reverse It

            //Create an array of at least 5 string elements
            string[] weekDays = new string[5] {"Monday","Tuesday","Wednesday","Thursday","Friday"};
            string[] reversedWeekDays = ReverseIt(weekDays);
            foreach (string item1 in weekDays) {
                Console.WriteLine(item1);
            }
            Console.WriteLine("\r\n");
            foreach (string day in reversedWeekDays) {
                Console.WriteLine(day);
            }
           
       

        }

        public static decimal GallonsNeeded (decimal w, decimal h, decimal area, int coats) {
            decimal gallons = (((w * h) / area) *coats);
            return gallons;
        }

        public static decimal NumberOfStings(decimal w) {
            decimal stings = w * 9;
            return stings;
        }

        public static string[] ReverseIt(string[] givenList) {
            int j = 0;
            string[] newList = new string[givenList.Length];
            for (int i = givenList.Length -1; i >= 0; i--) {               
                newList[j] = givenList[i];
                j++;           
            }
            return newList;
        }




    }
}
