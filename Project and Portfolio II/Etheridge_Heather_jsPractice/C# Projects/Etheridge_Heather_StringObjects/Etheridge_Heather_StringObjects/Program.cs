﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etheridge_Heather_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Heather Etheridge
            January 22, 2017
            Section #02
            String Objects
            */

            //Problem #1 Email Address Checker

            //Ask the user to enter an email address
            Console.WriteLine("Please enter an email address.");
            string email = Console.ReadLine();

            bool validOrNot = EmailCheck(email);
            if (validOrNot == true)
            {
                Console.WriteLine("The email address of {0} is a valid email address.", email);
            }
            else {
                Console.WriteLine("The email address of {0} is not a valid email address.", email);
            }
            /*
            Test Values

            test@fullsail.com
            The email address of test@fullsail.com is a valid email address.

            test@full@sail.com
            The email of test@full@sail.com is not a valid email address.

            test@full sail.com
            The email of test@full sail.com is not a valid email address.

            hnetheridge@fullsail.edu
            The email of hnetheridge@fullsail.edu is a valid email address.
            */



            //Problem #2 Seperator Swap Out

            Console.WriteLine("List colors seperated by a charachter or your choosing");
            string userList = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(userList))
            {
                Console.WriteLine("Please do not leave blank.  List 5 colors seperated by a charachter of your choosing");
                userList = Console.ReadLine();
            }
            Console.WriteLine("Which charachter did you use to seperate the colors?");
            string userCharachter = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(userCharachter) && userCharachter.Length > 1) {
                Console.WriteLine("Please do not leave blank and do not enter more than one charachter.\r\n Which charachter did you use to seperate the colors?");
                userCharachter = Console.ReadLine();
            }
            Console.WriteLine("Enter another charachter that you would like to use instead.");
            string otherCharachter = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(otherCharachter) && otherCharachter.Length > 1)
            {
                Console.WriteLine("Please do not leave blank and do not enter more than one charachter.\r\n Which charachter would you like to use instead?");
                otherCharachter = Console.ReadLine();
            }
            string newList = SwapOut(userList, userCharachter, otherCharachter);
            Console.WriteLine("The original string of {0} with the new seporator is {1}",userList,newList);
            




        }

        //Create a function that will check for a single @ at least one . after @

        public static bool EmailCheck(string email) {
            int firstAt = email.IndexOf("@");
            string substring = email.Substring(firstAt + 1);
            if (email.Contains('@') && !substring.Contains('@') && substring.Contains('.') && !string.IsNullOrWhiteSpace(email)) {
                return true;
            }
            return false;
        }

        public static string SwapOut(string list, string seperatorUsed, string newSeperator) {
            string newString = list.Replace(seperatorUsed, newSeperator);
            return newString;
        }

    }
}
