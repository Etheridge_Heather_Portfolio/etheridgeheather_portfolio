class NPC {
    constructor(name, attack, hp, itemsHeld){
        //Npc's name
        this.name = name;
        //How much hp do they remove when they attack
        this.attack = attack;
        //How many health points does the npc have
        this.hp = hp;
        //What kind of item will the npc drop if defeated
        this.itemsHeld = itemsHeld;
    }

}

let liv = new NPC("Liv", 20, 50, [potion, superPotion]);

let kiwiFight = new NPC("Kiwi", 25, 75, [potion, superPotion, superPotion]);

let evaFight = new NPC("Eva", 30, 65, [potion, potion, potion, maxPotion]);

let tristian = new NPC("Tristian", 10, 150, [potion]);