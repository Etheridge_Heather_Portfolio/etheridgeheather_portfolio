//File to hold Jobs

//class for job
class Job {
    constructor(title, attack, magic, hp){
        //Job title
        this.title = title;
        //Attack = How much damage do you do 
        this.attack = attack;
        //Magic Attack = How much damage do you do
        this.magic = magic;
        //users health points
        this.hp = hp;
    }

}

let warrior = new Job("warrior", 25, 10, 125);
let mage = new Job("mage", 10, 35, 90);
let rogue = new Job("rogue", 25, 35, 100);
 
