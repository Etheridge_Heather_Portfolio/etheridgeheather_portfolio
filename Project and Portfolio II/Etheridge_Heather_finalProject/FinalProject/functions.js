//Get users name, if empty or cancel chosen re prompt. 
function getUserName(){ 
    //save users name in varible
    while(true){
        let answer = prompt("Enter the name of your charachter:");
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank.  Try again");
            continue;
        }else{
            userName = answer;
            break;
        }
    }
}

//See if user wants to change their name
function changeName(){
        //Show user their current name
        console.clear();
        console.log(userName + ", huh, that's an interesting name.");
        //Prompt user to see if they want to change name
        //Verify answer to prompt
        while(true){
    
            let answer = prompt("Is that really your name?\r\n1. Yes\r\n2. No");
    
            if(answer == null || answer == undefined || answer.trim() == ""){
                alert("Do not choose cancel or leave blank. Try again.")
                continue; 
            }else if (answer == 1 || answer.toLowerCase().trim() === "yes"){
                break;
            }else if(answer == 2 || answer.toLowerCase().trim() === "no"){
                console.log("I'll give you  another chance to tell me your real name then.");
                getUserName();
                break;
            }else {
                alert("Please only select 1 or 2. Try again.");
                continue;
            }
        }
        console.log("\r\nAlright " + userName + ", it's nice to meet you!");
}

//Let the user select a job
function selectJob(){
    console.log("\r\nWell, " + userName + ", there are many types of adventurers that come through this world.");
    console.log("What type of adventurer are you?\r\n");

    //Validate user input
    while(true){
        //Options for Job Type
        let answer = prompt("Choose a job.  The job you choose will affect your starting stats:\r\n"+
        "1. Warrior - Higher Attack and HP\r\n"+
        "2. Mage - Higher Magic and Defense\r\n"+
        "3. Rogue - Higher Attack and Magic");

        //Validate user input and set player
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue; 
        }else if (answer == 1 || answer.toLowerCase().trim() === "warrior"){
            console.clear();
            console.log("\r\nNice, we could use some more warriors around here.");
            //Set userJob to warrior
            player = warrior;
            break;
        }else if(answer == 2 || answer.toLowerCase().trim() === "mage"){
            console.clear();
            console.log("\r\nNice, we could use some more mages around here.");
            //Set userJob to mage
            player = mage;
            break;
        }else if(answer == 3 || answer.toLowerCase().trim() === "rogue"){
            console.clear();
            console.log("\r\nNice, we could use some more rogues around here.");
            //set userJob to rogue
            player = rogue;
            break;
        }else{
            alert("Please only select 1, 2, or 3.  Try again.");
            continue;
        }
    }
}

//Player attack
function playerAttack(){
    while(true){
        //Attack Menu
        let answer= prompt("Pick an attack option\r\n"+
        "1. Physical Attack\r\n"+
        "2. Magic Attack");
        //Validate answer isnt blank or null
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue;
        //Physical Attack
        }else if(answer == 1 || answer.toLowerCase().trim() === "physical attack"){
            currentEnemy.hp -= player.attack
            console.clear();
            console.log("You are in a battle with " + currentEnemy.name + "!!!\r\n");
            console.log("Your HP: " + player.hp);
            console.log("Enemy HP: " + currentEnemy.hp);
            alert("You attacked " + currentEnemy.name + " and did " + player.attack + " damage!" );
            defeatedCheck();
            break;
        //Magic Attack    
        }else if(answer == 2 || answer.toLowerCase().trim() === "magic attack"){
            currentEnemy.hp -= player.magic
            console.clear();
            console.log("You are in a battle with " + currentEnemy.name + "!!!\r\n");
            console.log("Your HP: " + player.hp);
            console.log("Enemy HP: " + currentEnemy.hp);
            alert("You attacked " + currentEnemy.name + " and did " + player.magic + " damage!" );
            defeatedCheck();
            break;
        //Handle if user selects option not in menu    
        }else{
            alert("Please only select 1 or 2.  Try again.");
            continue;
        }
    }
}

//Enemy Attacks
function enemyAttack(){
    //enemy attacks player
    player.hp -= currentEnemy.attack;
    console.clear();
    console.log("You are in a battle with " + currentEnemy.name + "!!!\r\n");
    console.log("Your HP: " + player.hp);
    console.log("Enemy HP: " + currentEnemy.hp);
    alert(currentEnemy.name + " attacked you and did " + currentEnemy.attack + " dammage!");
    defeatedCheck();
    
}


//Check to see if either player or npc is defeated
function defeatedCheck(){
    if(player.hp <= 0){
        //If players hp reaches 0 or below they are defeated and game reloads
        alert("You have been defeated!\r\n"+
        "You will be reborn as a new adventurer without any memories of your past life....");
        location.reload(true);
    }else if(currentEnemy.hp <= 0){
        //If player defeats the enemy they get a random item that the enemy was holding
        console.log("\r\nYou have defeated " + currentEnemy.name + "!");
        let item = Math.round(Math.random() * (currentEnemy.itemsHeld.length-1))
        console.log("Congrats! You recieved a " + currentEnemy.itemsHeld[item].name + ".\r\n")
        inventory.push(currentEnemy.itemsHeld[item]);
        enemyDefeated = true;
    }
}

