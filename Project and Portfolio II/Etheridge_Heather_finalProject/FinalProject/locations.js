//This runs when a player enters Hothel
function hothel(){
    console.clear();
    console.log("Welcome to Hothel, we are a peacefull town where advenurers come to rest and relax.");
    console.log("Many of the townsfolk here are followers of the religion of Zen, therefore this town");
    console.log("is one of few that has a Church of Zen.  If you go to the church it is rumored that the \r\n"+
    "Goddess Zennifer will heal your wounds if you are deserving. ");
    //Let the user choose from the towns menu
    //Menu will loop unless Player travels to another town
    let running = true;
    while(running){
        hothelMenuActions();
    }
}

//Menu for the town of Hothel
function hothelMenu(){
    while(true){
        //Menu options to choose from 
        let answer = prompt("What would you like to do?\r\n"+
        "1. Inventory - View inventory\r\n"+
        "2. Church - Visit Church of Zen\r\n"+
        "3. Sophie - Talk to townsperson\r\n"+
        "4. Jennifer - Talk to townsperson\r\n"+
        "5. Evercrest - Go to the West\r\n"+
        "6. Evarice - Go to the South");

        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue; 
        }else if (answer.trim() == 1 || answer.toLowerCase().trim() === "inventory"){
            return 1;
            break;
        }else if (answer.trim() == 2 || answer.toLowerCase().trim() === "church"){
            return 2;
            break;
        }else if(answer.trim() == 3 || answer.toLowerCase().trim() === "sophie"){
            return 3;
            break;
        }else if(answer.trim() == 4 || answer.toLowerCase().trim() === "jennifer"){
            return 4;
            break;
        }else if(answer.trim() == 5 || answer.toLowerCase().trim() === "evercrest"){
            running = false;
            return 5;
            break;
        }else if(answer.trim()== 6 || answer.toLowerCase().trim() === "evarice"){
            running = false
            return 6;
            break;
        }else {
            alert("Please only select 1 - 5. Try again.");
            continue;
        }
    }
  
}

function hothelMenuActions(){
    switch(hothelMenu()){
        case 1://Check Inventory
            inventoryMenu();
            break;
        case 2://Go to Church
            churchOfZen();
            break;
        case 3://Talk to Sophie
            sophie();
            break;
        case 4://Talk to Jennifer
            jennifer();
        case 5://Go to Evercrest
            playerLocation = "Evarcrest";
            evarcrest();
            break;
        case 6://Go to Evarice
            playerLocation = "Evarice";
            evarice();
            break;
    }
}


function sophie(){
    console.clear();
    //Sophie introduces herself
    console.log("Hello there traveler, welcome to our small town.\r\n" +   
    "Not many adventurers live here although many pass through to rest and visit our church.\r\n")
    console.log("If theres anything you'd like to know, do not hold your tongue, I'd love to be of assistance.");
    //Menu to allow player to ask Sophie questions
    while(true){
        //Menu options to choose from 
        let answer = prompt("What would you like to know?\r\n"+
        "1. Church - Ask about the church of Zen\r\n"+
        "2. Tristian - Ask about the monster\r\n"+
        "3. Hothel - More information about the town of Hothel\r\n"+
        "4. Goodbye - End the conversation")
        //Verify input and act on player menu choice
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue; 
        }else if(answer == 1 || answer.toLowerCase().trim() === "church"){
            //Sophie gives information about the church
            console.clear();
            console.log("The Goddess Zennifer is a loving goddess of life.");
            console.log("She is responsible for everything from healing, to childbirth, and even bringing life to the flora and crops of this world\r\n");
            console.log("Many in this town worship her and we believe she is responisble for the prosperity Hothel enjoys.");
            console.log("It is said that if you've lived a life of kindness she will revive your health if you visit her church.");
            continue;
        }else if(answer == 2 || answer.toLowerCase().trim() === "tristian"){
            //Sophie gives information about the monster Tristian
            console.clear();
            console.log("*Sophies demeanor changes and she looks a bit frightened*\r\n");
            console.log("Oh, I'm sorry, It's just that even hearing his name fills me with dread....");
            console.log("When I was but a child, Tristian was just a story, a name that parents used to scare their children into staying close by...\r\n"+
            "But that story became reality not too many years ago.  To the north, there used to be a city called Light Hope\r\n");
            console.log("It had a large bustling population and many advancements were taking place there.\r\n");
            console.log("Then one day, the sky became black, and what could only be described as a monster decended from the sky.");
            console.log("He announced, in the most frightning voice anyone had ever heard, that his name was Tristian\r\n" + 
            "and that he was there to bring an end to all that lay before him.  He destroyed the entire city of Light Hope!!\r\n");
            console.log("That town has, since then, been known as Dark Scar.  It has to only be a matter of time before he destorys more towns and cities...");
            console.log("*You see tears welling up in Sophie's eyes and decide to not push her for more information about Tristian*");
            continue;
        }else if(answer == 3 || answer.toLowerCase().trim() === "hothel"){
            //Sophie gives information about the town of Hothel
            console.clear();
            console.log("This town is small, but full of life and hope.  We grow many crops here that are exported to other towns nearby.");
            console.log("Ivandale is full of adventuring spirits who love to fight and explore,"+
            "but that life isn't for everyone.  Some just want to raise families and enjoy a slower pace.  That's what Hothel is best at!");
            console.log("Hothel is such a beautiful place to live, but even if the simple life isn't what you're after, it's a great place to visit.");
            continue;
        }else if(answer == 4 || answer.toLowerCase().trim() === "goodbye"){
            //Player chooses to leave conversation
            alert("You wave goodbye to Sophie and explore the rest of Hothel.");
            break;
        }else {
            alert("Please only select 1 - 4. Try again.");
            continue;
        }
    }
}

//Function that runs when you choose to speak with Jennifer
function jennifer(){
    console.clear();
    //Jennifer introduces herself
    console.log("Hi, I haven't seen you around here before.\r\n" +   
    "My name is Jennifer.  I'm told I look a lot like the Goddess Zennifer but I don't see the resemblance at all.")
    console.log("\r\n*You look over at a statue of the Goddess and then back at Jennifer");
    console.log("*The resemblance is uncanny and it really makes you wonder*");
    console.log("\r\n*Jennifer adjusts a small child that she has on her hip*");
    console.log("This is my son Jay! I came here to start a family. It's the best decision I ever made!");
    console.log("Anyways, is there anything you wanted to ask me?");
    //Menu to allow player to ask Jennifer questions
    while(true){
        //Menu options to choose from 
        let answer = prompt("What would you like to know?\r\n"+
        "1. Jay - Ask about Jennifers son.\r\n"+
        "2. Zennifer - Ask about her resemblace\r\n"+
        "3. Goodbye - End the conversation")
        //Verify input and act on player menu choice
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue; 
        }else if(answer == 1 || answer.toLowerCase().trim() === "jay"){
            //Sophie gives information about the church
            console.clear();
            console.log("Before I came to this town I was told that I would never be able to carry a child.");
            console.log("I had heard rummors of this towns church and it's healing powers so I decided to move here.\r\n");
            console.log("When I visited the Church of Zen for the first time I was filled with an overwhealming feeling of joy.");
            console.log("\r\nShortly after, me and my husband Vic were able to concieve this little miracle!");
            console.log("*Jennifer pokes Jay on the nose and he laughs.  It's the cutest thing you've seen all day.*");
            continue;
        }else if(answer == 2 || answer.toLowerCase().trim() === "zennifer"){
            //Sophie gives information about the monster Tristian
            console.clear();
            console.log("Oh don't tell me you think I look like her too!\r\n");
            console.log("*Jennifer rolls her eyes*");
            console.log("\r\nI've actually become quite famous around here.  People think I'm some kind of reincarnation of the Goddess or something.");
            console.log("It's actually quite rediculous when you think about it.");
            console.log("It's not like I have healing powers or anything!");
            console.log("\r\nAlthough my crops do grow at record speeds....even in the winter....");
            continue;
        }else if(answer == 3 || answer.toLowerCase().trim() === "goodbye"){
            //Player chooses to leave conversation
            alert("You wave goodbye to Zennifer.....uhhh.....Jennifer, and explore the rest of Hothel.");
            break;
        }else {
            alert("Please only select 1 - 3. Try again.");
            continue;
        }
    }
}
    



//This runs when a player enteres Evarice
function evarice(){
    console.clear();
    console.log("Welcome to Evarice, here is where fighters come to spar and learn.");
    console.log("Be carefull to watch your HP here, you don't want to die!");
    console.log("\r\nBy winning fights here you can build up quite an inventory of potions.")
    //Let the user choose from the towns menu
    //Menu will loop unless Player travels to another town
    let running = true;
    while(running){
        evariceMenuActions();
    }
}

//Menu for the town of Evarice
function evariceMenu(){
    while(true){
        //Menu options to choose from 
        let answer = prompt("What would you like to do?\r\n"+
        "1. Inventory - View inventory\r\n"+
        "2. Kiwi - Talk to townsperson\r\n"+
        "3. Eva - Talk to townsperson\r\n"+
        "4. Hothel - Go to the North\r\n"+
        "5. Evarcrest - Go to the NorthEast");

        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue; 
        //If player chooses inventory
        }else if (answer.trim() == 1 || answer.toLowerCase().trim() === "inventory"){
            return 1;
            break;
        //If player chooses Kiwi
        }else if (answer.trim() == 2 || answer.toLowerCase().trim() === "kiwi"){
            return 2;
            break;
        //If player chooses Eva
        }else if(answer.trim() == 3 || answer.toLowerCase().trim() === "eva"){
            return 3;
            break;
        //If player chooses Hothel
        }else if(answer.trim() == 4 || answer.toLowerCase().trim() === "hothel"){
            running = false;
            return 4;
            break;
        //If player chooses Evarcrest
        }else if(answer.trim() == 5 || answer.toLowerCase().trim() === "evarcrest"){
            running = false;
            return 5;
            break;
        }else {
            alert("Please only select 1 - 5. Try again.");
            continue;
        }
    }
  
}

function evariceMenuActions(){
    switch(evariceMenu()){
        case 1://Check Inventory
            inventoryMenu();
            break;
        case 2://Talk to Kiwi
            kiwi();
            break;
        case 3://Talk to Eva
            eva();
            break;
        case 4://Go to Hothel
            playerLocation = "Hothel";
            hothel();
            break;
        case 5://Go to Evercrest
            playerLocation = "Evercrest";
            evercrest();
            break;
    }
}

//Function that runs when you choose to speak with Kiwi
function kiwi(){
    console.clear();
    //Kiwi introduces herself
    console.log("Hi, You look like you'd be fun to fight!\r\n" +   
    "My name is Kiwi.  I live in this town, an endless ammount of adventures come through for me to spar with,")
    console.log("so it's the perfect place for me to live!");
    console.log("\r\nIf it's not a fight you're looking for, then what do you want?!");
    //Menu to allow player to ask Kiwi questions
    while(true){
        //Menu options to choose from 
        let answer = prompt("What would you like to do?\r\n"+
        "1. Fight - Spar with Kiwi.\r\n"+
        "2. Evarice - Ask about this town\r\n"+
        "3. Goodbye - End the conversation")
        //Verify input and act on player menu choice
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue; 
        }else if(answer == 1 || answer.toLowerCase().trim() === "fight"){
            //Sets up fight with Kiwi
            console.clear();
            currentEnemy = kiwiFight;
            //Attacks enemy, if they are not defeated they attack you
            //Repeates until enemy is defeated
            while(!enemyDefeated){
                playerAttack();
                if(!enemyDefeated){
                    enemyAttack();
                }
            }
            //You defeat Kiwi
            console.clear();
            console.log("You defeated me?!  I demand a rematch!");
            console.log("*Kiwi's face is extremely red.  She is either very angry or embarassed*");
            alert("Congrats on your win adventurer, be sure to heal up if you took damage or else risk death!");
            //resets enemy defeated
            enemyDefeated = false;
            continue;
        }else if(answer == 2 || answer.toLowerCase().trim() === "evarice"){
            //Kiwi gives information about Evarice
            console.clear();
            console.log("Everyone who lives here loves to fight! It's a great place to train.");
            console.log("There used to be a ton of monsters here, but we defeated them all long ago,");
            console.log("and now the only thing we have left to fight is each other!");
            continue;
        }else if(answer == 3 || answer.toLowerCase().trim() === "goodbye"){
            //Player chooses to leave conversation
            alert("You wave goodbye to Kiwi.  She sure has spirit.");
            break;
        }else {
            alert("Please only select 1 - 3. Try again.");
            continue;
        }
    }
}

//Function that runs when you choose to speak with Eva
function eva(){
    console.clear();
    //Eva introduces herself
    console.log("If you're here in Evarice you must be looking for a fight!" +   
    "My name is Eva. I didn't choose to come here, I was born here.  Whether or not I like fighting,")
    console.log("I've become quite good at it...");
    console.log("My parents love it here. They deny it, but I think they named me after this town...");
    console.log("You keep looking at me, is there somthing you want??");
    //Menu to allow player to ask Eva questions
    while(true){
        //Menu options to choose from 
        let answer = prompt("What would you like to do?\r\n"+
        "1. Fight - Spar with Eva.\r\n"+
        "2. Kiwi - Ask about Kiwi\r\n"+
        "3. Goodbye - End the conversation")
        //Verify input and act on player menu choice
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue; 
        }else if(answer == 1 || answer.toLowerCase().trim() === "fight"){
            //Sets up fight with Eva
            console.clear();
            currentEnemy = evaFight;
            //Attacks enemy, if they are not defeated they attack you
            //Repeates until enemy is defeated
            while(!enemyDefeated){
                playerAttack();
                if(!enemyDefeated){
                    enemyAttack();
                }
            }
            //You defeat Eva
            console.clear();
            console.log("Well... I never said I enjoyed fighting.");
            console.log("If you're looking for a real fight, go talk to my sister.");
            alert("Congrats on your win adventurer, be sure to heal up if you took damage or else risk death!");
            //resets enemy defeated
            enemyDefeated = false;
            continue;
        }else if(answer == 2 || answer.toLowerCase().trim() === "kiwi"){
            //Eva gives information about Kiwi
            console.clear();
            console.log("Kiwi is my sister.  She loves a good fight, how do you think I got good at sparing.");
            console.log("All she ever does is fight, this town is perfect for her.");
            console.log("When I'm a bit older, I think I'll move to Hothel, I'll miss Kiwi terribly though.");
            continue;
        }else if(answer == 3 || answer.toLowerCase().trim() === "goodbye"){
            //Player chooses to leave conversation
            alert("You wave goodbye to Eva. She seems very kind.");
            break;
        }else {
            alert("Please only select 1 - 3. Try again.");
            continue;
        }
    }
}

//This runs when a player enteres Evarcrest
function evarcrest(){
    console.clear();
    console.log("Welcome to Evarcrest, It is a gloomy town close to Dark Scar");
    console.log("The people are hopefull that Dark Scar will be revived if Tristian is ever defeated.");
    console.log("\r\nDark Scar is close, beware, if you choose to venture there, you cannot turn back!!!");
    //Let the user choose from the towns menu
    //Menu will loop unless Player travels to another town
    let running = true;
    while(running){
        evarcrestMenuActions();
    }
}

//Menu for the town of Evarcrest
function evarcrestMenu(){
    while(true){
        //Menu options to choose from 
        let answer = prompt("What would you like to do?\r\n"+
        "1. Inventory - View inventory\r\n"+
        "2. Denise - Talk to townsperson\r\n"+
        "3. Dark Scar - Go to the NorthEast\r\n"+
        "4. Evarice - Go to the SouthEast")

        //Act on player choice
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue; 
        //If player chooses inventory
        }else if (answer.trim() == 1 || answer.toLowerCase().trim() === "inventory"){
            return 1;
            break;
        //If player chooses Denise
        }else if (answer.trim() == 2 || answer.toLowerCase().trim() === "denise"){
            return 2;
            break;
        //If player chooses Dark Scar
        }else if(answer.trim() == 3 || answer.toLowerCase().trim() === "dark scar"){
            running = false;
            return 3;
            break;
        //If player chooses Evarice
        }else if(answer.trim() == 4 || answer.toLowerCase().trim() === "evarice"){
            running = false;
            return 4;
            break;
        }else {
            alert("Please only select 1 - 4. Try again.");
            continue;
        }
    }
  
}

//Acts on player choice in Evarcrest
function evarcrestMenuActions(){
    switch(evarcrestMenu()){
        case 1://Check Inventory
            inventoryMenu();
            break;
        case 2://Talk to Denise
            denise();
            break;
        case 3://Go to Dark Scar
            playerLocation = "Dark Scar";
            darkScar();
            break;
        case 4://Go to Evarice
            playerLocation = "Evarice";
            evarice();
            break;
    }
}

//Function that runs when you choose to speak with Denise
function denise(){
    console.clear();
    //Denise introduces herself
    console.log("Hi, I'm not sure why you would come to this sorry excuse for a town....\r\n" +   
    "No one ever comes here....unless....wait....")
    console.log("\r\nDon't tell me you're going to try and defeat Tristain!");
    console.log("*Denise looks both excited and worried*");
    console.log("\r\nIf that is your plan you must be very careful.");
    console.log("If you're worried you can try sparring in Evarice first to get used to battles and earn potions.");
    console.log("You can also try healing yourself first at the Church of Zen in Hothel");
    console.log("\r\nAnyways, is there anything you wanted to ask me?");
    //Menu to allow player to ask Denise questions
    while(true){
        //Menu options to choose from 
        let answer = prompt("What would you like to know?\r\n"+
        "1. Dark Scar - Ask about Dark Scar\r\n"+
        "2. Evarcrest - Ask about this town\r\n"+
        "3. Goodbye - End the conversation")
        //Verify input and act on player menu choice
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.")
            continue; 
        }else if(answer == 1 || answer.toLowerCase().trim() === "dark scar"){
            //Denise gives information about Dark Scar
            console.clear();
            console.log("Dark Scar used to be a beautiful and flourishing city known as Light Hope.");
            console.log("That was before Tristian arrived though...");
            console.log("He completely destroyed the city and now it is always overcast by black clouds.");
            console.log("\r\nPlease! You must do something, you look like you could handle it!");
            continue;
        }else if(answer == 2 || answer.toLowerCase().trim() === "evarcrest"){
            //Denise gives information about Evarcrest
            console.clear();
            console.log("Evarcrest used to just be empty dirt fields...");
            console.log("but after Tristian destroyed Light Hope, any survivors set up makeshift homes here.");
            console.log("\r\nIt may not be the best of places, but we make due.");
            console.log("Everyone here is hopeful that if Tristain is defeated we may be able to return home.");
            console.log("All we can do is hope...");
            console.log("*Denise stares off into the distance, you can tell she's been through a lot*");
            continue;
        }else if(answer == 3 || answer.toLowerCase().trim() === "goodbye"){
            //Player chooses to leave conversation
            alert("You wave goodbye to Denise...You have to help her and everyone else!");
            break;
        }else {
            alert("Please only select 1 - 3. Try again.");
            continue;
        }
    }
}

//Dark Scar
//The final area
function darkScar(){
    console.clear();
    console.log("You finally arrive at Dark Scar...");
    console.log("It is just as people described it to you, they sky is full of black clouds,"+
    "vand all the buildings have been reduced to rubble");
    console.log("\r\n*You hear a demoic voice*");
    console.log("You, a mere mortal, dare to enter my lair!!");
    console.log("Just who do you think you are?!");
    console.log("Your mere pressence annoys me, so I will make sure to end this quickly.");
    currentEnemy = tristian;
    //Attacks enemy, if they are not defeated they attack you
    //Repeates until enemy is defeated
    while(!enemyDefeated){
        playerAttack();
        if(!enemyDefeated){
            enemyAttack();
        }
    }
    //Player Wins!!!
    console.clear();
    console.log("Nooooooo!!!! How could this have happened?!");
    console.log("*Tristian screams as his body turns to ash*");
    console.log("\r\nThe clouds part and you see a beautiful sky filled with sunshine.");
    console.log("It will be a long journey but the city of Light Hope is once again free.'");
    console.log("\r\nYou can hear the cheers from the people of Evarcrest as they lift you up and start chanting your name");
    console.log(userName.toUpperCase() + "! " + userName.toUpperCase() + "! " + userName.toUpperCase() + "! ");
    console.log("\r\nYou have become the hero that the people of Ivandale deserve.");
    alert("You have defeated Tristian and won the game!!!  Feel free to refresh and play again!");
    return;
}



//Church of Zen  
//This function can be reused for other towns that also
//hava a church
function churchOfZen(){
    console.clear();
    console.log("You enter the church of Zen and take a look around.\r\n");
    console.log("There is a painting of a beautiful woman on the wall and the look on her face alone makes you feel\r\n"+
    "like everything is right in the world.  This must be the Goddess Zennifer....\r\n");
    console.log("The air arround you suddently feels very calm and you feel your wounds heal.");
    player.hp = 200;
    alert("Your health has been returned to full and you leave the church");
}

//Inventory
//Will show what is in inventory and give player chance to 
//either use item or exit inventory menu
function inventoryMenu(){
    //variable to hold menu number that prefaces menu item
    let menuNum;
    //variable to hold string for menu
    let title = "Select an item from your inventory\r\n"
    //In the console explain what different items do
    console.clear();
    console.log("Select an item from your inventory to use or choose exit.\r\n");
    console.log("Potions heal 25 HP");
    console.log("Super Potions heal 50 HP");
    console.log("Max Potions bring HP to max which is 200 HP");
    //Loop to build string that will be used in prompt
    //Takes index of array and adds one to get menu number
    //Then adds menu num plus item held in array to build 
    //inventory menu
    while(true){
        //Resets menuString to title
        let menuString = title;
        //Prints Menu
        for(let i = 0; i < inventory.length; i++){
            menuNum = i + 1;
            menuString += menuNum + ". " + inventory[i].name + "\r\n";
        }
        menuString += (inventory.length + 1) + ". Exit"
        //Gets input 
        let answer = prompt(menuString);
        //Checks answer to see if null or empty
        if(answer == null || answer == undefined || answer.trim() == ""){
            alert("Do not choose cancel or leave blank. Try again.");
            continue;
        //Checks if player chose exit    
        }else if((answer == inventory.length+1) || answer == "exit"){
            return;
        }
        for(let i = 0; i < inventory.length; i++){
            //checks input for null or empty
            //Takes action if player chooses item
            if((answer == i + 1) || answer == inventory[i].name.toLowerCase()){
                //Uses chosen item
                alert("You used a " + inventory[i].name + " and healed " + inventory[i].recovers + " HP!" );
                player.hp += inventory[i].recovers
                //Can't overheal, so if HP goes above 200 which is max HP
                //HP will be set to 200
                if(player.hp > 200){
                    player.hp = 200;
                }
                //Removes item from inventory
                inventory.splice(i,1);
                break;
            }
            //Takes action if player enters something other than menu options
            alert("Please only select one of the options from the menu. Try again.");
        }
    }
}
