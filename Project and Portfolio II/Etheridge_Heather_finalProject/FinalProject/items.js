//File holds objects that represent items in the game

let potion = {
    name : "Potion",
    recovers : 25
}

let superPotion = {
    name : "Super Potion",
    recovers : 50
}

let maxPotion = {
    name : "Max Potion",
    recovers : 200
}
