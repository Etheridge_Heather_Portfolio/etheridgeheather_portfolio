//Global variables
let userName;
let player;
let playerLocation;
let npc;
let inventory = [];
let currentEnemy;
let enemyDefeated;


//Introduce user to the game
alert("To play this game, open the JavaScript Console.\r\n" +
"The game text will be in the console and prompts will be in the pop ups.");
console.log("Hello adventurer! Welcome to Ivandale!");
console.log("My name is Marsa, I keep watch over this world and the adventurers that pass through.");
//Ask for users name
console.log("I haven't seen you here before, What is your name?");
//Get users name
getUserName();
//Give user a chance to change name.
changeName();
//Let the user select a class
selectJob();


//Explain a bit about the game to the user

console.log("\r\nIn the world of Ivandale, adventurers explore and travel though different places.");
console.log("Durring your travels you will meet many kinds of people...both good and bad.\r\n");
console.log("At the far end of the world lies a terrible beast who has been terrorizing Ivandale,");
console.log("His name is Tristian, and none have yet defeated him...");      
console.log("\r\nYou may not be strong enough to defeat him right now, but by putting effort into your adventure,");
console.log("I am certain that you will be the one to finally bring peace to Ivandale!");
console.log("\r\nBeware though, if you should perish, you shall be reborn as an a new adventurer with no memories of your past life.....");
console.log("Select where you would like to go first!");

//Give user the option of which city they would like to visit first
while(true){
    let answer= prompt("Where would you like to go next?\r\n"+
    "1. Hothel - A peaceful town to the north\r\n"+
    "2. Evarice - A place where fighters train");

    //Validate answer and set userLocation
    if(answer == null || answer == undefined || answer.trim() == ""){
        alert("Do not choose cancel or leave blank. Try again.")
        continue; 
    }else if(answer == 1 || answer.toLowerCase().trim() === "hothel"){
        console.log();
        //Set userLocation to Hothel
        playerLocation = "Hothel";
        break;
    }else if(answer == 2 || answer.toLowerCase().trim() === "evarice"){
        console.log();
        //Set userJob to mage
        playerLocation = "Evarice";
        break;
    }else{
        alert("Please only select 1 or 2.  Try again.");
        continue;
    }
}

//Print the users health at the top of the screen.  
console.clear();
console.log("Current HP: " + player.hp);
//On the way to chosen location meet an NPC
console.log("\r\nOn the way to " + playerLocation + " you run into Liv.");
//Live introduces herself and explains the fighting system
console.log("\r\nHey there, My name is Liv.  You look like a pretty strong " + player.title + ".");
console.log("It's been a while since I've had a good fight, we should battle!");
console.log("I'm holding 2 items right now and if you beat me you will recieve one of them at random.");
console.log("Choose an option to attack!");
currentEnemy = liv;
//Attacks enemy, if they are not defeated they attack you
//Repeates until enemy is defeated
while(!enemyDefeated){
    playerAttack();
    if(!enemyDefeated){
        enemyAttack();
    }
}
//You defeat Liv and she explains the item system to you.
console.log("Wow, you really defeated me!  You looked pretty strong but I didn't expect that at all.");
console.log("You picked up one of my items too.  Dang it!");
console.log("\r\nWhat?!, youre actually new to all of this? I cant belive it!");
console.log("Well, that item you picked up should have gone into your inventory automatically,");
console.log("\r\nYou can only access your inventory while you're in a town....");
console.log("I mean...If you went digging through your bag out here you'd get robbed for sure!");
console.log("\r\nI still cant belive you beat me, I need to keep training.");
console.log(playerLocation + " is just up ahead.  Good luck " + userName + "!\r\n"+ 
"Maybe YOU will be the one who defeats Tristian and brings peace back to Ivandale!");
alert("Go on your way to " + playerLocation);
//Resets enemy defeated to false
enemyDefeated = false;
playerLocation == "Hothel" ? hothel() : evarice();






