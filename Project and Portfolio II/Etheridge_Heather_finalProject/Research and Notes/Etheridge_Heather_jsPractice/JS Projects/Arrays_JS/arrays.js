             /*
             Heather Etheridge
             Section 02
             January 8, 2017
             Arrays Assignment
             */


            //Declare and Define The Starting Number Arrays
            var firstArray = [ 4, 20, 60, 150 ];
            var secondArray = [ 5, 40.5, 65.4, 145.98 ];

            //Find the total of each array and store it in a variable and output to console
            var totalFirstArray = firstArray[0] + firstArray[1] + firstArray[2] + firstArray[3];
            var totalSecondArray = secondArray[0] + secondArray[1] + secondArray[2] + secondArray[3];
            alert("The total of the first array is " + totalFirstArray +  " and the total of the secondArray is " + totalSecondArray + ".");

            //Calculate the average of each array and store it in a variable and output to console
            //Just a reminder to check the averages with a calculator as well, to make sure they are correct.
            var averageFirstArray = totalFirstArray / firstArray.length;
            var averageSecondArray = totalSecondArray / secondArray.length;
            alert("The average of the first array is " +averageFirstArray+ " and the average of the second array is " +averageSecondArray+ ".");

            /*
               Create a 3rd number array.  
               The values of this array will come from the 2 given arrays.
                -You will take the first item in each of the 2 number arrays, add them together and then store this sum inside of the new array.
                -For example Add the index#0 of array 1 to index#0 of array2 and store this inside of your new array at the index#0 spot.
                -Repeat this for each index #.
                -Do not add them by hand, the computer must add them.
                -Do not use the numbers themselves, use the array elements.
                -After you have the completed new array, output this to the Console.
             */
            var thirdArray = new Array(4);
            thirdArray[0] = firstArray[0] + secondArray[0];
            thirdArray[1] = firstArray[1] + secondArray[1];
            thirdArray[2] = firstArray[2] + secondArray[2];
            thirdArray[3] = firstArray[3] + secondArray[3];
            alert("When adding our previous two arrays to get a third array, the result is: [ " + thirdArray[0] + ", " + thirdArray[1] + ", " + thirdArray[2] + ", " + thirdArray[3] + " ]");


            /*
               Given the array of strings below named MixedUp.  
               You must create a string variable that puts the items in the correct order to make a complete sentence.
                -Use each element in the array, do not re-write the strings themselves.
                -Concatenate them in the correct order to form a sentence.
                -Store this new sentence string inside of a string variable you create.
                -Output this new string variable to the console.
             */

            //Declare and Define The String Array
            var mixedUp = [ "but the lighting of a", "Education is not", "fire.", "the filling", "of a bucket," ];

            //Create a string variable that puts the items in the correct order
            var legibleSentence = mixedUp[1] + " " + mixedUp[3] + " " + mixedUp[4] + " " + mixedUp[0] + " " + mixedUp[2];

            //Output this new sting to the console
            alert(legibleSentence);

        
    