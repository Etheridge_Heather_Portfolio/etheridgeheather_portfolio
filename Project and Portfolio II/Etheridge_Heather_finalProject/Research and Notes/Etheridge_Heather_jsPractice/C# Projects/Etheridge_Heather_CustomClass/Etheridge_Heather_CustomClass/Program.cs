﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etheridge_Heather_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Heather Etheridge
            January 25, 2017
            Section 2
            Custom Class
            Animal Shelter Class
            */

            //Variables
            int dogsAdded;
            int dogsRemoved;

            //Instantiate the custom class as an object
            AnimalShelter southSideShelter = new AnimalShelter( );

            //Introduce users to program and start loop
            for (int i = 0; i< 7; i++) {
                Console.WriteLine("\r\nWelcome to {0}! There are currently {1} dogs here and we can have up to {2} total.", southSideShelter.GetShelterName(), southSideShelter.GetCurrentNumber(), southSideShelter.GetHighestNumber());
                Console.WriteLine("Have any dogs been added or removed from the shelter?\r\nReply added if dogs have been added or removed if dogs have been removed.");
                string addOrRemove = Console.ReadLine().ToLower();
                while (!addOrRemove.Contains("added") && !addOrRemove.Contains("removed")) {
                    Console.WriteLine("Only enter added or removed. Reply added if dogs have been added or removed if dogs have been removed.");
                    addOrRemove = Console.ReadLine();
                }
                if (addOrRemove.Contains("added"))
                {
                    Console.WriteLine("How many dogs were added? Please only enter whole numbers.");
                    string dogsAddedString = Console.ReadLine();
                    while (!int.TryParse(dogsAddedString, out dogsAdded)) {
                        Console.WriteLine("Please only enter whole numbers. How many dogs were added?");
                        dogsAddedString = Console.ReadLine();
                    }
                    southSideShelter.SetAddDog(dogsAdded);
                    southSideShelter.AddDog();
                }
                else {
                    Console.WriteLine("How many dogs were removed? Please only enter whole numbers");
                    string dogsRemovedString = Console.ReadLine();
                    while (!int.TryParse(dogsRemovedString, out dogsRemoved)) {
                        Console.WriteLine("Please only enter whole numbers.  How many dogs were removed?");
                        dogsRemovedString = Console.ReadLine();
                    };
                    southSideShelter.RemoveDog(dogsRemoved);
                }



            }
        }
    }
}

