﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etheridge_Heather_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Heather Etheridge
            January 15, 2017
            Section 02
            Logic Loops
            Scalable Data Infrastructures
            */


            //Problem #1 Logical Operators-Tire Pressure

            //Prompt the user for the pressure of the first tire
            Console.WriteLine("What is the pressure of your front left tire?");
            //Save Response
            string frontLeftString = Console.ReadLine();
            //Declare variable to hold converted value
            double frontLeft;
            //Make a while loop to validate user input is a number
            while (!double.TryParse(frontLeftString, out frontLeft))
            {
                //Inform the user of the issue
                Console.WriteLine("You have typed something other than a number.\r\nPlease enter a number:");
                //Recapture the users response
                frontLeftString = Console.ReadLine();
            }

            //Prompt the user for the pressure of the second tire
            Console.WriteLine("What is the pressure of your front right tire?");
            //Save Response
            string frontRightString = Console.ReadLine();
            //Declare variable to hold converted value
            double frontRight;
            //Make a while loop to validate user input is a number
            while (!double.TryParse(frontRightString, out frontRight))
            {
                //Inform the user of the issue
                Console.WriteLine("You have typed something other than a number.\r\nPlease enter a number:");
                //Recapture the users response
                frontRightString = Console.ReadLine();
            }

            //Promt the user for the pressure of the third tire
            Console.WriteLine("What is the pressure of your back left tire?");
            //Save Response
            string backLeftString = Console.ReadLine();
            //Declare variable to hold converted value
            double backLeft;
            //Make a while loop to validate user input is a number
            while (!double.TryParse(backLeftString, out backLeft))
            {
                //Inform the user of the issue
                Console.WriteLine("You have typed something other than a number.\r\nPlease enter a number:");
                //Recapture the users response
                backLeftString = Console.ReadLine();
            }

            //Prompt the user for the pressure of the last tire
            Console.WriteLine("What is the pressure of your back right tire?");
            //Save Response
            string backRightString = Console.ReadLine();
            //Declare variable to hold converted value
            double backRight;
            //Make a while loop to validate user input is a number
            while (!double.TryParse(backRightString, out backRight))
            {
                //Inform the user of the issue
                Console.WriteLine("You have typed something other than a number.\r\nPlease enter a number:");
                //Recapture the users response
                backRightString = Console.ReadLine();

            }

            //Create an array to store the 4 tire pressures
            double[] tireArray = new double[4] { frontLeft, frontRight, backLeft, backRight };

            //Create a conditional to see if the tires pass spec
            if ((tireArray[0] == tireArray[1]) && (tireArray[2] == tireArray[3]))
            {
                Console.WriteLine("The tires pass spec!");
            }
            else
            {
                Console.WriteLine("Get your tires checked out!");
            }

            /*
            Test Values
            Front Left 32, Front Right 32, Back Left 30, Back Right 30 = Tires pass Spec
            Front Left 36, Front Right 32, Back Left 26, Back Right 25 = Get your tires checked
            Front Left 38, Front Right 38, Back Left 38, Back Right 38 = Tires pass Spec                                  
            */


            //Write line just to give some seperation between programs
            Console.WriteLine();


            //Problem #2 Logical Operators-Movie Ticket Price

            //Let the user know what we are doing and prompt for input
            Console.WriteLine("Hello! To find out the price of your movie ticket, first please enter your age:");
            //Capture user response
            string ageString = Console.ReadLine();
            //Create a variable to hole conversion
            int age;
            //Validate that user entered a number
            while (!int.TryParse(ageString, out age))
            {
                //Let the user know about the error
                Console.WriteLine("Please only enter numbers and do not leave blank.\r\nWhat is your age?");
                //Recapture response
                ageString = Console.ReadLine();
            }
            //Ask the user for the time of their movie
            Console.WriteLine("What is the time of your movie using whole numbers in military time?");
            //Capture the response
            string timeString = Console.ReadLine();
            //Create a variable to hold conversion
            int time;
            //Validate user response
            while (!int.TryParse(timeString, out time))
            {
                //Let the user know about the error
                Console.WriteLine("Please only enter numbers and do not leave blank.\r\nWhat is the military time of your movie in whole numbers?");
                //Recapture the response
                timeString = Console.ReadLine();
            }
            //Create a conditional to determine the price of the tickets
            if ((age >= 55 || age < 10) || (time >= 14 && time <= 17))
            {
                //Let the user know the price of the ticket
                Console.WriteLine("The ticket price is $7.00");
            }
            else
            {
                Console.WriteLine("The ticket price is $12.00");
            }

            /*
            Test Values
            Age 57, Time 20 = $7
            Age 9, Time 20 = $7
            Age 38, Time 20 =$12
            Age 25, Time 16 =$7
            Age 10, Time 12 = $12
            */

            //Just a line between programs for readablility
            Console.WriteLine();


            //Problem #3 For Loop:Add up the odds or evens

            //Create an array of 6 or more integers
            int[] intArray = new int[7] { 1, 4, 7, 8, 10, 17, 24 };

            //Ask the user if they would like to see the sum of even or odd numbers
            Console.WriteLine("For the array {1,4,7,8,10,17,24}\r\nWould you like to see the sum or even numbers of the sum of odd numbers?");
            Console.WriteLine("Reply even or odd.");
            //Capture users response
            string evenOrOdd = Console.ReadLine();
            //Validate user response
            while (evenOrOdd != "even" && evenOrOdd != "odd") {
                //Let the user know about the error
                Console.WriteLine("Please only enter even or odd in all lowercase letters and do not leave blank.");
                evenOrOdd = Console.ReadLine();
            }
            //Create a variable to hold the sum
            int sum = 0;

            //Create a conditional to add up even or odds
            if (evenOrOdd == "even") {
                for (int i = 0; i < 7; i++) {
                    if (intArray[i] % 2 == 0) {
                        sum += intArray[i];
                    }
                }
            } else {
                for (int i = 0; i < 7; i++) {
                    if (intArray[i] % 2 == 1) {
                        sum += intArray[i];
                    }
                }
            }

            //Let the user know the sum of either even or odds based on their prompt
            Console.WriteLine("The sum of all {0} numbers in this array is {1}.", evenOrOdd, sum);

            //Space between programs
            Console.WriteLine();


            //Problem #4-Charge It!

            //Let the user know what the program will do and prompt them for their credit limit
            Console.WriteLine("We will help you spend the most you can with your credit card.  What is your credit limit?");
            //Capture the response
            string creditLimitString = Console.ReadLine();
            //Create a variable to hold the conversion
            decimal creditLimit;
            //Verify user input
            while (!decimal.TryParse(creditLimitString, out creditLimit)) {
                //Let the user know about the error
                Console.WriteLine("Please only enter numbers and do not leave blank.\r\nWhat is your credit limit?");
                //Recapture response
                creditLimitString = Console.ReadLine();
            }
            //Create a variable to hold the total purchase ammount
            decimal purchaseSum = 0;
            //Create a variable to hold each purchase ammount
            string currentPurchaseString;
            //Create a loop that will keep track of purchase sum and prompt for each purchase ammount
            while (purchaseSum <= creditLimit) {
                //Ask the user for their purchase price
                Console.WriteLine("What is the ammount of your purchase?");
                //Capture the response
                currentPurchaseString = Console.ReadLine();
                //Var to hold conversion
                decimal currentPurchase;
                //validate response
                while (!decimal.TryParse(currentPurchaseString, out currentPurchase)) {
                    Console.WriteLine("Please only enter numbers and do not leave blank.\r\nWhat is the price of your purchase?");
                    //recapture response
                    currentPurchaseString = Console.ReadLine();
                }
                //calculate new sum
                purchaseSum += currentPurchase;
                //calculate money left to spend
                decimal leftToSpend = creditLimit - purchaseSum;
                //Let the user know how much they have left to spend
                Console.WriteLine("With your current purchase of {0}, you can still spend {1}", currentPurchase.ToString("C"),leftToSpend.ToString("C"));          
            }
            //exceeded by var
            decimal exceedBy = purchaseSum - creditLimit;
            if (purchaseSum > creditLimit) {
                Console.WriteLine("With your last purchase you have reached your credit limit and exceeded it by {}.",exceedBy);
            }
        }

    }





}
    
