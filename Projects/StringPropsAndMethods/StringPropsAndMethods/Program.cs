﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringPropsAndMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            //String Properties and Methods

            //Create a string variable
            string food = "cheese pizza";

            //Lets Try string property of length
            //Length will tell you how many charachters make up a text string
            Console.WriteLine(food.Length);

            //String Methods

            //IndexOf()
            Console.WriteLine(food.IndexOf("pizza"));
            Console.WriteLine(food.IndexOf("meat"));
            //You can also add which index number you want to start searching
            Console.WriteLine(food.IndexOf("pizza", 3));
            //If you start the search after your string starts you can't find it
            Console.WriteLine(food.IndexOf("pizza", 8));

           
            string sentence = "Madam I'm Adam";
            int firstAdam = sentence.IndexOf("adam",2);
            Console.WriteLine("Adam first occurs at index {0}.",firstAdam);
            //LastIndexOf()
            //Finds the last occurrence of a parameter
            //IS CASE SENSITIVE
            int lastAm = sentence.LastIndexOf("am");
            Console.WriteLine("The last indext of am is {0}",lastAm);

            //ToUpper() ToLower() 
            //Will change the charachgers is a string to either all capital or lowercase
            Console.WriteLine(sentence.ToLower());
            Console.WriteLine(sentence.ToUpper());
            //Writing a new variable to hold the conversion will not affect the original
            string newSentence = sentence.ToUpper();
            Console.WriteLine(newSentence);
            Console.WriteLine(sentence);
            //If you do wish to affect the original
            sentence = sentence.ToLower();
            Console.WriteLine(sentence);

            //Substring()
            //Substrings can search between 2 index numbers indexA and index B
            string ultimateAnswer = "Life, The Universe, and everything ";
            //If you want to seperate it so that you just have The Universe 
            //Find the first comma and take everything afer
            int firstComma = ultimateAnswer.IndexOf(",");
            Console.WriteLine(firstComma);       
            //Since that includes the comma you can just add one to it to get rid of it?
            //Well dont forget that the space counts as an index as well so you have to add 2
            string theSubString = ultimateAnswer.Substring(firstComma+2,12);
            Console.WriteLine(theSubString);

            //Trim()
            //Returns the string without the leading and trailing spaces
            //It DOES NOT change the original string
            //You can either use a returned value variable to save the change
            //Or use the original string and reasign the value. Ex:
            // var myString = myString.Trim();
            string groceryList = "Apple, Banana, Strawberry";
            Console.WriteLine(groceryList.Trim());
            //Find the first comma
            //Adding plus one to include the space as well 
            int firstCommaGroc = groceryList.IndexOf(",")+1;
            Console.WriteLine(firstCommaGroc);
            //Find the next comma "Last" comma
            int lastCommaGroc = groceryList.LastIndexOf(",");
            Console.WriteLine(lastCommaGroc);
            //Find the length between commas
            int lengthOfItem = lastCommaGroc - firstCommaGroc;
            //Substring of grocery list stored in a new variable
            string shortList = groceryList.Substring(firstCommaGroc, lengthOfItem).Trim();
            Console.WriteLine(shortList);

            //Split a string into an array of substrings 
            string teachersNames = "Williams, Dan; Carroll, Rebecca; Lewis, Lee; Wainman, Scott;";
            //Create an array to hold the substrings
            string[] teacherArray = teachersNames.Split(';');
            //Cycle through the array using a foreach loop
            foreach (string teacher in teacherArray ) {
                Console.WriteLine(teacher.Trim());
            }


            //Sort names alphabetically         
            //Just like strings have methods arrays are also objects with methods
            //Sort is a method of arrays
            Array.Sort(teacherArray);
            Console.WriteLine("\r\n");
            foreach (string teacher in teacherArray)
            {
                Console.WriteLine(teacher.Trim());
            } 











        }
    }
}
