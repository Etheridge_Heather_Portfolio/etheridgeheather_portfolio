﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtheridgeHeather_FindErrorsCond
{
    class Program
    {
        static void Main(string[] args)
        {
            //  NAME:  Heather
            //  DATE:  January 14, 2017
            // Section 02
            // Scalable Data Infrastructures
            //  Find and fix the errors

            String myName = "John Doe";
            String myJob = "Cat Wrangler";

            double myRatePerCat = 7.50;
            double catsWrangled = 1;




            int numberOfCats = 40;
            bool employed = true;

            Console.WriteLine("Hello!  My name is " + myName + ".");
            Console.WriteLine("I'm a " + myJob + ".");
            Console.WriteLine("My current assignment has me wrangling" + numberOfCats + " cats.");
            Console.WriteLine("So, let's get to work!");

            while (numberOfCats > 0)
            {

                if (employed == true)
                {
                    double totalPay = myRatePerCat * catsWrangled;
                    Console.WriteLine("I've wrangled another cat and I have made $" + totalPay + " so far.  \r\nOnly " + numberOfCats + " left!");

                }
                else
                {

                    Console.WriteLine("I've been fired!  Someone else will have to wrangle the rest!");
                    break;

                };

                numberOfCats--;
                catsWrangled++;

                if (numberOfCats <= 5)
                {

                    employed = false;

                }
            }
        }
    }
}
