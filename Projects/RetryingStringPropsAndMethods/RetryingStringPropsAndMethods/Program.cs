﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetryingStringPropsAndMethods
{
    class Program
    {
        static void Main(string[] args)
        {
 
            //.Length() method
            //Length will tell you how many charachters make up a text string

            //Create a string variable
            string food = "cheese pizza";
            Console.WriteLine(food.Length);

            //Trying out some String Methods

            //.IndexOf() method
            Console.WriteLine(food.IndexOf("pizza",8));

            string sentence = "Madam I'm Adam";

            int firstAdam = sentence.IndexOf("adam");
            Console.WriteLine("Adam first occurs at index {0}.", firstAdam );

            //Last Index of 
            int lastAm = sentence.LastIndexOf("Ad");

            Console.WriteLine("The last index of am is {0}",lastAm );

            //to upper to lower
            Console.WriteLine(sentence.ToUpper());

            
            //You can save the output in a new variable
            //This does not change the original
            string newSentence = sentence.ToUpper();
            Console.WriteLine(newSentence);
            Console.WriteLine(sentence);
            //If you DID want to affect the original 
            //just reasign the variable
            sentence = sentence.ToLower();
            Console.WriteLine(sentence);



            //.SubString() method
            //Returns part of a string that already exists

            //Creata a new string
            string ultimateAnswer = "Life, the universe, and everything.";

            //If we didn't want the life part, just the universe, and everything
            //You could find the first comma and take everything after
            //create a variable to catch returned value
            int firstComma = ultimateAnswer.IndexOf(",");
            Console.WriteLine(firstComma);

            //Use substring to take just what you want from a string
            //Need paramaters of index A and an optional index B
            //Starting point is index A ending point is index B
            //Need to add 2 becuase we start at the comma
            //IMPORTANT index B does not start at the original index
            //it uses index A as it's new index 0
            string theSubstring = ultimateAnswer.Substring(firstComma+2);
            Console.WriteLine(theSubstring);
            //So if you want to just have the string "the universe"
            //you would need to use optional index B
            string theUniverse = ultimateAnswer.Substring(firstComma+2,12);
            Console.WriteLine(theUniverse);


            //.Trim() method
            //gets rid of leading and trailing spaces

            string groceryList = "    Apple, Banana, Strawberry   ";
            Console.WriteLine(groceryList.Trim());
            //Find the first comma in the list
            int firstCommaGroc = groceryList.IndexOf(",")+1;
            //You have to add the +1 so that it will not start with the comma
            //You just want the words by themselves
            //Not the commas
            Console.WriteLine(firstCommaGroc);
            //Substring of grocery list
            //You can stack the .syntax like so
            //string shortList = groceryList.Substring(firstCommaGroc).Trim();
            //Console.WriteLine(shortList);
            //Lets say we want to get the word banana by itself
            //We can then find the second comma 
            //Find the next comma
            int lastCommaGroc = groceryList.LastIndexOf(",");
            Console.WriteLine(lastCommaGroc);
            //We need to find the length between commas so we can use it as index B
            int lengthOfItem = lastCommaGroc - firstCommaGroc;
            string shortList = groceryList.Substring(firstCommaGroc, lengthOfItem).Trim();
            Console.WriteLine(shortList);




            //.Split() method
            //Splits a string into an array of substrings
            //You designate a seperator like a , ; " 
            //If you leave out the parameter of the seperator it will seperate at the spaces

            //Create a list with seperators
            string teachersNames = "Williams, Dan; Carrol, Rebecca; Lewis, Lee; Wainman, Scott";
            //Create an array to hold the substrings
            string[] teacherArray = teachersNames.Split(';');
            //This created the array
            //To see it, cycle through the array using a for each loop
            //the first parameter in the for each loop is you creating a variable
            //to hold each item in the array
            foreach (string teacher in teacherArray) {
                Console.WriteLine(teacher.Trim());
            }

            //Let's say we want to put the names in alphabetical order
            //You can do that by using an array method
            //Just like strings have methods, so do arrays, so do many objects
            //How to sort by alpabetical order .Sort() array method
            Array.Sort(teacherArray);
            //Bellow is just a space to put between list
            Console.WriteLine("\r\n");
            //Keep in mind that sorting actually does change the original
            foreach (string teacher in teacherArray) {
                Console.WriteLine(teacher.Trim());
            }

       


        

    


        }
    }
}
