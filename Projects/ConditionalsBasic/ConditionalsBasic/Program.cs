﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConditionalsBasic
{
    class Program
    {
        static void Main(string[] args)
        {
            //Boolean variables 
            //Only contain a value of true or false

            //True is not a text string of "true"
            // bool testing1 = "true"; does not work
            //Think of it as on or off

            bool testing1 = true;

            bool wontrun = false;

            Console.WriteLine(testing1);
            Console.WriteLine(wontrun);


            //Relational Operators
            Console.WriteLine(5<7);
            Console.WriteLine(7>5);

            Console.WriteLine(2>5);

            Console.WriteLine(5<=5);    //Returns true
            Console.WriteLine(5<5);     //Returns false

            //Equality
            // == two equal signs with no space  checks if something is equal to something else
            //Inequality
            // != exclamation and equal signs with no space checks to see if something is inequal

            Console.WriteLine(4==4);        //true
            //Console.WriteLine(4=="4") doesn't work because you cant compare different data types

            Console.WriteLine(4!=4);        //false


            //If statement
            //Else statement
            if (true) {
                Console.WriteLine("This code will run if the boolean is true.");
            }

            if (false) {
                Console.WriteLine("This code will not run if the boolean is false.");
            }

            if (5 > 8)
            {
                Console.WriteLine("This code will not run because 5 is not greater than 8.");
            } else {
                Console.WriteLine("This line will run when the boolean is false.");
            }
          
            //Else if
            // You can use else if to test a series of conditions until you get to ann else "catch all" statement

            //Declare and define a variable
            int kidAge = 11;

            if (kidAge >= 13) {
                Console.WriteLine("The child can go to the PG-13 movie.");

            } else if (kidAge >= 10) {
                Console.WriteLine("The child can go to the PG movie.");

            } else {
                Console.WriteLine("The child can only see G rated movies.");

            }



            //We work in a shoe store
            //We get a bonus if we sell 50 or more pairs of shoes in a day

            //Declare and define starting variables

            decimal basePay = 200.00m;
            decimal bonus = 50.00m;

            //What is our total pay at the end of the day?
            decimal totalPay = 0.00m;

            //How many pairs of shoes did we sell?
            int shoesPerDay = 35;

            //Create a conditional to test if we get our bonus
            if (shoesPerDay >= 50)
            {
                //We get our bonus
                //total pay should be our base pay plus bonus pay
                totalPay = basePay + bonus;

            }
            else {
                //We do not get a bonus
                //Total pay is same as base pay
                totalPay = basePay;
            }

            Console.WriteLine("Your total pay is $" + totalPay);



        }

    }
}
