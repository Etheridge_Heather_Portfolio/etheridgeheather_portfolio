﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoorLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            //For Loops
            //for(variable;test;increment of change;){}
            for (int counter = 0; counter < 11; counter++) {
                Console.WriteLine("The value of the counter is {0}.", counter);

                //if we get to 7 then break it
                if (counter==7) {
                    Console.WriteLine("Stopping the loop.");
                    break;
                }

            }

            //break will stop the loop totally when it is reached
            //continue will only skip that one iteration of the loop

            //create for loop
            for (int i = 0; i < 50; i++) {

                //Skip every other step
                if (i % 2 == 0) {
                    //Is the number even?
                    //This will skip all even numbers
                    continue;
                }
                Console.WriteLine("The value of i is {0}.", i);
            }





            //For each loops
            //Create an array
            int[] myBills = new int[4] { 30, 40, 50, 60 };
            //Console.WriteLine(myBills[0]);

            //Create a variable to hold the sum
            int totalSum = 0;

            //Create the foreach loop
            foreach (int arrayItem in myBills) {
                Console.WriteLine("The item in the array is {0}.", arrayItem);
            }

            foreach (int eachBill in myBills) {
                //For each bill in the array we will add it to the total sum
                totalSum = totalSum + eachBill;
                //Shortcut for this
                //totalSum += eachBill;
            }
            Console.WriteLine("The sum of our bills is {0}", totalSum);

        }
    }
}
