﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace moreArays
{
    class Program
    {
        static void Main(string[] args)
        {
            //Function call to run our array lists method
            nameArrayLists();
        }

        public static void nameArrayLists() {
            //Create an array list
            //And add an initial element
            ArrayList studentList = new ArrayList() {"Dan"};

            //.Add() method
            //adds elements to the array
            //Keep in mind that it will always add the new element to the END of the array list

            //Let's add some new students to our array list
            studentList.Add("Craig");
            studentList.Add("Josh");
            studentList.Add("Chad");
            studentList.Add("Rebecca");

            //.Insert() method
            //Lets you specify where in the array a new element goes
            //.Insert(int, element)
            studentList.Insert(0, "Lee");
            //This will insert Lee into the 0 index slot and move everything else down

            //.Remove() method
            //Removes a specified element
            //Let's remove a student
            studentList.Remove("Chad");
            //If you try to remove something that doesn't exist the program just ignores it and still runs
            studentList.Remove("Bob");

            //.Clear() method
            //will clear the whole array's contents wipes them out
            //good for reseting or redoing something
            //studentList.Clear();

            //.Contains() method
            //will check for an element and returns true or false
            //true if it's in the array list false if it's not
            //Let's see if the array list contains an element
            Console.WriteLine("Contains? {0}",studentList.Contains("Josh"));
            //Returns true becuase Josh is in the array list
            //What happens if we look for something not in the list?
            Console.WriteLine("Contains? {0}", studentList.Contains("Bob"));



            //Just console.write the array list wont work, you have to loop through it 
            //Create a foreach loop to cycle through our array
            foreach (string student in studentList) {
                //Write out the names
                Console.WriteLine("Student: {0}",student);
            }


            //Array list properties
            //use the index #
            //To get a specific element
            Console.WriteLine(studentList[2]);

            //You can set an element the same way.
            studentList[3] = "Chris";
            Console.WriteLine(studentList[3]);
            //if there is already something in the index of 3 it changes it, it does not add the new element and move things down
            //Be careful of this because if you add more elements it could change which 
            //Watch so that you are not out of range as well

            //Get the number of elements in the array list
            Console.WriteLine("Count: "+ studentList.Count);

            //Capacity
            Console.WriteLine("Capacity: " + studentList.Capacity);
            //capacity finds the number of elements that the array list is ABLE to store
            //Count is how many elements are currently inside of it
            //


        




        }

    }
}
