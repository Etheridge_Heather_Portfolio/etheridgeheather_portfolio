﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextVariables
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables - Charachters

            //Charachters - char- only the size of ONE charachter
            // Must be surrounded by single quotes ''

            //Declare a variable
            //char firstLetter;
            char firstLetter;

            //Define a variable
            //firstLetter = 'A';
            firstLetter = 'A';

            //Print the variable to our console
            //Console.WriteLine(firstLetter);
            Console.WriteLine(firstLetter);

            //Declare AND define a variable      
            //char secondCharachter = 'b';
            char secondCharachter = 'b';

            //Print the new variable
            //Console.WriteLine(secondCharachter);
            Console.WriteLine(secondCharachter);

            //String of charachters -string- basically anything bigger than a single charachter
            // Must be surrounded by double quotes ""
            //string wholeSentence = "This is an example of a sentence.";
            string wholeSentence = "This is an example of a sentence.";

            //Print out the string variable
            //Console.WriteLine(wholeSentence);
            Console.WriteLine(wholeSentence);
            //Combine Strings together
            // Use plus sign - Concatination
            string combinedString = "First Part " + "Second Part";
            Console.WriteLine(combinedString);

            //Spaces?
            string firstName = "Kermit";
            string lastName = "The Frog";
            string wholeName = firstName + " " + lastName;

            Console.WriteLine(wholeName);
        }
    }
}
