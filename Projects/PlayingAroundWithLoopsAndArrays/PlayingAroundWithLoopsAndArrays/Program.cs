﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayingAroundWithLoopsAndArrays
{
    class Program
    {
        static void Main(string[] args)
        {

            //Create an array to loop through
            int[] myBills = new int[4] {30,40,50,60};

            //Create a variable to hold the sum
            int totalSum = 0;

            //Create for each loop
            //foreach (datatype, name item to take on each item inside the array, keyword in, name of array)
            foreach (int arrayItem in myBills) {
                //each item in the array will loop
                Console.WriteLine("The item in the array is {0}.", arrayItem);
                //will continue to loop as long as there are items in the array
            }

            foreach (int eachBill in myBills) {
                //For each bill, we will add it to the total sum
                totalSum = totalSum + eachBill;
                //shortcut way
                //totalSum += eachBill;
            
            }

            Console.WriteLine("The sum of our bills is {0}.",totalSum);
            //So this is a great way to add up items in an array


            //break will stop a loop totaly
            //continue will only skip that itration of the loop

            for (int i = 0; i<50; i++) {

                if (i%2 == 0) {
                    //Checks if the number is even
                    //if a number diveded by 2 has zero as a remainder it is even :)
                    continue;
                    //the continue will skip every iteration in which i is even....yaaaassss                    
                }
                Console.WriteLine("The value of i is {0}.", i);
            }


            //TEST

            int[] numbers = new int[4] {1,2,25,34};

            int sum = 0;

            for (int i = 0; i < 4; i++) {
                if (numbers[i] % 2 == 0)
                {
                    continue;  
                }
                sum += numbers[i];
            }

            
               
            

        }
    }
}
