﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Heather Etheridge
            November 14, 2016
            Total Project
            SDI
            Section 02           
            */

            //Calculate the area of a rectangle from user prompts
            //Tell the user what we are doing and ask for width
            Console.WriteLine("Hello! We are going to find the area and perimeter of a rectangle.\r\nPlease type in a value for the width and hit enter.");
            //Capture the users response
            string widthString = Console.ReadLine();
            //Declare a variable to hold the converted value
            double width;
            //Validate the user is typing in a valide number using a while loop
            while (!double.TryParse(widthString, out width)) {
                //Alert the user to the error
                Console.WriteLine("Please only type in numbers and do not leave blank.\r\nWhat is the width of the rectangle.");
                //Re-Capture the users response in the same variable as before
                widthString = Console.ReadLine();

            }
            //Tell the user the width and ask for a length
            Console.WriteLine("Got it! You entered a width of " + width + ". What is the length?");
            //Create a variable to capture the users resoponse
            string lengthString = Console.ReadLine();
            //Declare a variable to hold the converted value
            double length;
            //Validate the user is typing in a valid number using a while loop
            while (!double.TryParse(lengthString, out length)) {
                //Alert the user to the error
                Console.WriteLine("Please only type in numbers and do not leave blank.\r\nWhat is the length of the rectangle.");
                //Recapture the users response in the same variable as before
                lengthString = Console.ReadLine();
            }
            //Tell the user that we got the length and let them know the next step
            Console.WriteLine("Thank you.  You entered a width of {0} and a length of {1}.\r\nWe will now calculate the perimeter", width, length);
            //Go create a function to calculate the perimiter outside of the main function
            //Function call the CalcPeri method
            //Remember to catch the ruturned value with a variable AND use arguments
            double perimeter = CalcPeri(width, length);
            //Report to the user
            Console.WriteLine("The perimeter of the rectangle is " + perimeter + ".");
            //Create a function to calculate the area outside of the main function
            //Function call the CalcArea method and catch the returned value
            double area = CalcArea(width, length);
            //Report to the user
            Console.WriteLine("The area of the rectangle is {0}.", area);
            //Tell the user we want to find the volume and ask for height
            Console.WriteLine("Let's find the volume if the rectangle was actually a rectangular prism.\r\nWhat is the height?");
            //Create a variable to hold height
            string heightString = Console.ReadLine();
            //Declare the height variable
            double height;
            //Validate the users response
            while (!double.TryParse(heightString, out height)) {
                //Alert the user to the error
                Console.WriteLine("Please onlyl type in numbers and do not leave blank.\r\nWhat is the height of the rectangular prism?");
                //Recapture the users response in the same variable as before
                heightString = Console.ReadLine();
            }
            //Tell the user
            Console.WriteLine("You typed in a height of {0}, We will now find the volume!", heightString);
            //Create another function for volume outside of the main function
            //Fuction call and save the returned value
            double volume = CalcVol(width, length, height);
            //Final output to the user
            Console.WriteLine("The volume of the rectangular prism is {0}.\r\nThank you for using this program!", volume);

            /*Test Values
            * width =4
            * length = 5
            * height = 6
            *
            * Computer calculated these values:
            * perimiter = 18
            * area = 20
            * volume = 120
            * All values are checked with a calculator and correct
            */



        }
        //Here is our function to calculate the perimeter
        public static double CalcPeri(double wid, double len) {
            //Create a variable for the perimeter and do the math
            double peri = 2 * wid + 2 * len;
            //Return the parimeter value
            return peri;
        }
        //Here is our function to calculate the area
        public static double CalcArea(double wid, double len) {
            //Create a variable for the area and do the math
            double area = wid * len;
            //Return the area value
            return area;
        }
        //Here is our function to calculate the volume
        public static double CalcVol(double w, double l, double h) {
            double volume = w * l * h;
            return volume;
        }
    
    }
}
