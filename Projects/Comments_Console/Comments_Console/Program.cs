﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comments_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Heather Etheridge
                SDI Section #
                Commenting and Consoles
                October 29, 2016
            */

            //  This is a single line comment.
            /*
                This is a multi line comment!
                Just another line.
            */

            //Console.WriteLine() - will put a text string onto the console AND a new line charachter at the end of it!
            Console.WriteLine("This will print to the console.");
            Console.WriteLine("This is an example of a second line.");

            //Console.Write() - no new line charachter.
            Console.Write("This is an example of a console write.\r\n");
            Console.Write("Another line of text.");
            Console.Write("\r\n");
            Console.Write("Welcome to SDI!");

            //Cariage Return \r
            //New Line Charachter \n
            //We will us for a new line charachter /r/n




        }

    }
}
