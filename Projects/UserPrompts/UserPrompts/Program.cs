﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserPrompts
{
    class Program
    {
        static void Main(string[] args)
        {
            //ReadLine method of the Console
            //Method is something the Console can do
            //Reads the next line in the Console and returns the text string back to your code

            //ONLY returns a string variable datatype!!

            //Ask the user their name

            //Ask the useers the question first
            //Then listen for response

            Console.WriteLine("Please type in your name and then press enter.");

            //Listen for the answer
            //Create a variable to "catch" the returned text string
            string userName = Console.ReadLine();

            //Say Hello to the user
            Console.WriteLine("Hello " + userName + ", welcome to our program!");

            //Ask the user for a number
            //Calculate the perimiter or a rectangle
            // width *2 + length*2
            //Ask the user for width and length
            //Don't forget to tell the user what you're doing
            //Reminder: the \r\n creates a new line 

            Console.WriteLine("Let's find out the perimeter of a rectangle!\r\nPlease enter a width and press return.");

            //Listen for width as a text string

            string widthString = Console.ReadLine();

            //Convert the string to a number data type
            double widthNumber = double.Parse(widthString);

            //Ask the user for a length
            Console.WriteLine("Now please enter a length");
            string lengthString = Console.ReadLine();
            double lengthNumber = double.Parse(lengthString);

            //Use to create a perimeter

            double perimeterRect = widthNumber * 2 + lengthNumber * 2;

            Console.WriteLine("The perimeter of the rectangle is " + perimeterRect + ".");

            //With current code if user types in decimal place the code will break because we used int. 
            //We changed int to double so that it could be an int with or without decimal places




        }
    }
}
