﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetryingBasicFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Before the function call.");

            //Create a function call that will start our PrintMore function
            //Put the name of the function and ();
            PrintMore();

            Console.WriteLine("After the functoin call.");

            //A second function call
            PrintMore();

            //A functio call to the PrintEnjoy funcion
            Console.WriteLine("Before enjoy function call.");
            PrintEnjoy();

            string userName = "Bob";
            Console.WriteLine(userName);
        }

        public static void PrintEnjoy() {
            Console.WriteLine("I hope that you enjoy this program!");

        }


        public static void PrintMore()
        {
            //Code that will run when I "Call" this method/function
            string userName = "Heather";
            Console.WriteLine("Welcome {0} to the program!",userName);
           
        }



    }
}
