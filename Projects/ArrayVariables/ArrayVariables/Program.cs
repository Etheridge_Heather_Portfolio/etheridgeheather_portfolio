﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayVariables
{
    class Program
    {
        static void Main(string[] args)
        {
            //Arrays are signified by []
            //datatype arrayName = new datatype[size of the array];

            //Create the array - define the array
            int[] bills = new int[3];

            //Fill or deifine the array
            //arrayName[index#] = value;
            //Index # always starts with zero!!
            //Index # of the last item is one less than the size of the array

            bills[0] = 130;
            bills[1] = 150;
            bills[2] = 2000;

            //Use the array item
            Console.WriteLine("The water bill for this month is $" +bills[1] );
            Console.WriteLine("The electric bill was $" + bills[0]);
            
            //Let's do math with arrays
            //Total of bills
            int totalBills = bills[0] + bills[1] + bills[2];
            Console.WriteLine("The total bills for the month are $" + totalBills);

            //Declare AND Define an array at the same time
            //datatype[] arrayName = new string[] {value1, value2, value3, etc...};
            string[] fruits = new string[] {"apple","pear","banana", "grapes" };
            Console.WriteLine(fruits[2]);

            //To find out how big an array is
            //Length property
            //Dot syntax - use a perios
            Console.WriteLine(fruits.Length);

            //Change any value inside an array
            //stays changed - it will be the new value for the rest of the code unelss it's changed again
            Console.WriteLine(fruits[0]);
            //variableName = new value
            fruits[0] = "kiwi";
            Console.WriteLine(fruits[0]);
            

        }
    }
}
