﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetryingTotalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Heather Etheridge
            January 18, 2017
            Section 02
            Total Project         
            */

            //Calculate the area and perimeter of a rectangle from user prompts
            //Tell the user what we are doing and ask for width
            Console.WriteLine("Hello, we are going to find the area and perimeter of a rectangle\r\nPlease type in a value for the width and hit enter.");
            //Capture the users response with a variable
            string widthString = Console.ReadLine();
            //Declare a variable to hold the converted value
            double width;
            //Validate the user is typing in a valid number using while loop
            while (!double.TryParse(widthString, out width)) {
                //Let the user know about the error
                Console.WriteLine("Please only enter numbers and do not leave blank.\r\nWhat is the width?");
                //Recapture the response in the SAME variable
                widthString = Console.ReadLine();
            }
            //Tell the user the width and ask for a length
            Console.WriteLine("Got it! You entered a width of {0}.\r\nWhat is the length?", width);
            //Create a variable to capture the users response
            string lengthString = Console.ReadLine();
            //Declare the variable to hold the converted value
            double length;
            //Validate using a while loop
            while (!double.TryParse(lengthString, out length)) {
                //Alert the user of the error
                Console.WriteLine("Please only enter numbers and do not leave blank.\r\nWhat is the length");
                //Recapture response
                lengthString = Console.ReadLine();
            }

            //Tell the user that we got the length and tell them next step
            Console.WriteLine("Thank you, you entered a width of {0} and a length of {1}.\r\nWe will now calculae the perimeter.", width, length);

            //Go create a function to calculate the perimeter

            //Function call the CalcPeri method
            //Remember to catch the returned value with a variable and use arguments
            double perimeter = CalcPeri(width, length);
            //Report to the user
            Console.WriteLine("The perimeter of the rectangle is {0}.", perimeter);

            //Now create a function to calculate the area
            //Function call and store the returned value
            double area = CalcArea(width, length);
            //Report it to the user
            Console.WriteLine("The area of the rectangle is {0}.", area);

            //Tell the user we want to find the volume and ask for height   
            Console.WriteLine("Let's find the volume, if the rectangle is actually a rectangular prism!\r\nWhat is the height?");
            //Variable to hold height
            string heightString = Console.ReadLine();
            //Declare variable to hold conversion
            double height;
            //Validate user response
            while (!double.TryParse(heightString, out height)) {
                Console.WriteLine("Please enter a number and do no leave blank.\r\nWhat is the height of the rectangular prism?");
                heightString = Console.ReadLine();
            }
            Console.WriteLine("You typed in a height of {0}.  We will now find the volume!");

            //Call the method and store the return
            double volume = CalcVolume(width, length, height);
            //return data to user
            Console.WriteLine("The volum of the rectangular prism is {0}.", volume);
        }

        public static double CalcPeri(double w, double l) {
            //Create a variable for perimeter
            double perimeter = 2 * w + 2 * l;
            //Return the perimeter value
            return perimeter;
        }

        public static double CalcArea(double w, double l) {
            //Create a variable for the area
            double area = w * l;
            //Return the area value
            return area;
        }

        public static double CalcVolume(double w, double l, double h) {
            double volume = w * l * h;
            return volume;
        }
    }
}
