﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberVariables
{
    class Program
    {
        static void Main(string[] args)
        {
            //Numeric Data Types
            //Integers
            //sbyte - signed byte - positive or negative
            sbyte exSbyte = 50;
            Console.WriteLine(exSbyte);

            //Short
            short exShort = 1000;
            Console.WriteLine(exShort);

            //integer - int
            int exInt = 6000000;
            Console.WriteLine(exInt);

            //Long
            long exLong = 400000000000000;
            Console.WriteLine(exLong);

            //Double
            double exDouble = 67.1234;
            Console.WriteLine(exDouble);

            //Float
            float exFloat = 13.3456f;
            Console.WriteLine(exFloat);

            //Decimal
            decimal exDecimal = 16.89M;
            Console.WriteLine(exDecimal);

            //Unsigned - Only Positive Values
            byte exByte = 245;
            Console.WriteLine(exByte);

            //Ushort
            ushort exUshort = 45023;
            Console.WriteLine(exUshort);

            //Uint
            uint exUint = 4000000000;
            Console.WriteLine(exUint);

            //Ulong
            ulong exUlong = 560000000000000;
            Console.WriteLine(exUlong);


        }
    }
}
