﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create our first box
            //Call the constructor function
            Box firstBox = new Box(5,5,5,"Red");

            //Second Box
            Box secondBox = new Box(6,7,8,"Blue");

            //Can we access the member variables directly?
            //Do not make them public, they should be private
            /* 
            Console.WriteLine(firstBox.mHeight);
            firstBox.mHeight = 10;
            Console.WriteLine(firstBox.mHeight);
             */

            //Access information about the box
            //Hve to use Getter method
            //Create this in the custom class file
            Console.WriteLine("The height of our first box is " + firstBox.GetHeight());

            //Lets use all of our Getter Methods
            Console.WriteLine("The width of our first box is " + firstBox.GetWidth());
            Console.WriteLine("The length of our first box is " + firstBox.GetLength());
            Console.WriteLine("The color of our first box is " + firstBox.GetColor());

            //What does the second box look like
            Console.WriteLine("The height of our second box is " + secondBox.GetHeight());
            Console.WriteLine("The width of our second box is " + secondBox.GetWidth());
            Console.WriteLine("The length of our second box is " + secondBox.GetLength());
            Console.WriteLine("The color of our second box is " + secondBox.GetColor());

            //Change the value of height for our first box
            firstBox.SetHeight(-10);
            Console.WriteLine("\r\nThe height of our first box after the setter is " +firstBox.GetHeight());

            //Change the value of height for our second box
            secondBox.SetHeight(2);
            Console.WriteLine("The height of our second box after the setter is " + secondBox.GetHeight());

            //Use the rest of the setters
            firstBox.SetLength(10);
            firstBox.SetWidth(10);
            firstBox.SetColor("Yellow");

            Console.WriteLine("\r\nThe length of the first box is "+firstBox.GetLength());
            Console.WriteLine("The width of the first box is "+firstBox.GetWidth());
            Console.WriteLine("The color of the first box is "+firstBox.GetColor());

            //Call on our custom Class Method
            Console.WriteLine("The volume of the first box is "+firstBox.FindVolume());
            Console.WriteLine("The volume of the second box is "+secondBox.FindVolume());

            //Function call our Surface Area Function
           
            Console.WriteLine("The surface area of the first box is "+firstBox.FindSurfaceArea());
            Console.WriteLine("The surface area of the second box is "+secondBox.FindSurfaceArea());

            firstBox.SetHeight(10);

            Console.WriteLine("The volume of the first box is "+firstBox.FindVolume());

            //Call the constructor function with no color choice
            Box thirdBox = new Box(6,6,6);
            //Call the Getters
            Console.WriteLine("\r\nThe length of our third box is "+thirdBox.GetLength());
            Console.WriteLine("The width of our third box is "+thirdBox.GetWidth());
            Console.WriteLine("The height of our third box is "+thirdBox.GetHeight());

            //We didnt assign it a color, so what happens when we try to get it's color?
            Console.WriteLine("The color of our third box is "+thirdBox.GetColor());
            //It just returns a blank because we didn't set the color
            //There are two ways to fix this
            //Either set the color
            //thirdBox.SetColor("Pink");
            Console.WriteLine("The color of our third box is " + thirdBox.GetColor());
            //The other way to fix this would be to add a default color if none is set


        }
    }
}
