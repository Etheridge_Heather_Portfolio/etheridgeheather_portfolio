﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClass
{
    class Box
    {
        //Properties of the box class go here
        //Member Variable - Same thing as a property
        //It is standard proactice to start the variable names with an m
        //These are global variables, only for this class though
        //Member variables are all private by nature
        //They cannot be seen in the main class
        //Unless you add the public in front of the variable
        //Not good practice to put public in front of the variables
        //Because they can then be changed in the main method

        int mLength;
        int mWidth;
        int mHeight;
        string mColor;

        //Create a constructor function
        //Add parameters for the member variables
        //suggested naming conventino is _name
        public Box(int _length, int _width, int _height, string _color) {

            //Set the values of the member variables using the parameters
            mLength = _length;
            mWidth = _width;
            mHeight = _height;
            mColor = _color;

        }

        //Method Overloading

        //You can create a class with the same name so that you can use
        //optional parameters in you main method
        //Duplicate the function with different paramaters
        //could be another number or even another data type
        //Create another constructor function with no color choice

        public Box(int _length, int _width, int _height) {
            //Set the values of the member values using the parameters
            mLength = _length;
            mWidth = _width;
            mHeight = _height;

            //set a default color if none is chosen
            //this is optional
            mColor = "Gray";
        }





        //Create a getter method for the height of the cube
        //Getters should ALWAYS be public 

        public int GetHeight() {
            //We don't need parameters
            //We just need to return the value of height
            return mHeight;
        }

        //Create Getter Methods for each of the member variables
        public int GetWidth() {
            return mWidth;
        }
        public int GetLength() {
            return mLength;
        }
        public string GetColor() {
            return mColor;
        }


        //Create Setter Methods for each of the member variables

        //Unlike the Getters, which don't need a parameter
        //The Setters DO need a parameter
        //Standard naming is _name
        //Sometimes you will see something like this:
        //this.mHeight
        //The keyword "this" refers to the current instance of the class
        //You only want to make the setters for things you thnk will get changed later
        public void SetHeight(int _height) {
            //Add a conditional to test if negative
            if (_height > 0)
            {
                //Change the value of the member variable
                this.mHeight = _height;
            }
            else {
                //Alert the user to not use negative numbers
                Console.WriteLine("This value cannot be negative.\r\n Please try again.");
            }
        }
        public void SetWidth(int _width) {
            this.mWidth = _width;
        }
        public void SetLength(int _length) { 
           this. mLength = _length;
        }
        public void SetColor(string _color) {
           this. mColor = _color;
        }

        //Create a custom method
        //One for volume, one for surface area
        //You don't need parameters because the values we are going to be using
        //are going to come from the member variables for each individual object

        public int FindVolume() {
            //Use the member variables inside of our calculations
            //volume = width*length*height;
            int volume = mWidth * mLength * mHeight;
            return volume;
        }

        //Create a function for calculating surface area

        public int FindSurfaceArea() {
            //Use member variables in our calculations
            //Surface Area = 2(w*l) + 2(w*h) + 2(l*h)

            int sArea = 2 * (mWidth * mLength) + 2 * (mWidth * mHeight) + 2 * (mLength * mHeight);
            return sArea;
        }


    }
}
