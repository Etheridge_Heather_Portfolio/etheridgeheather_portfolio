﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedStringConCatenation
{
    class Program
    {
        static void Main(string[] args)
        {
            double myNum = 12.345;
            Console.WriteLine("The value is " + myNum);

            //Use a placeholder or parameter{index #}
            //Index starts at 0 Zero

            string test = String.Format("The meaning of life is {0}",42);
            Console.WriteLine(test);

            double otherNum = 56.7;
            Console.WriteLine("The first number is {0} and the second number is {1}.", myNum, otherNum);
            Console.WriteLine("Just adding a line to test file changes in git.");
            Console.WriteLine("One more line for git practice between OS's.");


        }
    }
}
