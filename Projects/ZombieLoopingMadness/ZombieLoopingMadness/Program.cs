﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieLoopingMadness
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zombie Madness!

            //Givens First
            //Start with 1 zombie
            //Each zombies can bite 4 people a day
            //8 days

            //Turn into variables

            //How many zombies do we have?
            int numZombies = 1;
            //Number of bites per zombie per day
            int numBites = 4;
            //How many days?
            int days = 8;

            //Create for loop to cycle through each day
            for (int i=1; i<= days; i++) {
                //What happens in a day?
                //How many NEW zombies get created
                //4*number of zombies we have
                int newZombies = numZombies * numBites;
                //At end of day these new zombies joint the zombie hoard
                //Update number of zombies
                numZombies += newZombies;
                //Tell the public how many zombies we have each day
               Console.WriteLine("There are {0} zombies on day #{1}!", numZombies, i);


            }

            //How long will it take to reach 1,000,000 zombies?
            int numDays = 1;
            int zombieHordeNumber = 1;

            while (zombieHordeNumber <= 1000000) {

                //Happens each day
                int bittenPeople = zombieHordeNumber * numBites;
                //End of day those people become zombies
                zombieHordeNumber += bittenPeople;
                //Report to the people how many zombies there are.
                Console.WriteLine("On day #{0}, There are {1} zombies!", numDays, zombieHordeNumber);
                //At the end of the day we increase the day number
                numDays++;

            }

            for (int counter = 0; counter <= 5; counter++)
            {
                Console.WriteLine("I love C#");
            }




        }
    }
}
