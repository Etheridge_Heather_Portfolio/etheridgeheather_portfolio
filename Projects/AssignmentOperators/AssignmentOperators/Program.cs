﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            //Assignment Operators
            /* 
            = Assignment Operator - It means "is"
            ++ Addition of 1 to the value  a=a+1  is the same as a++
            -- Subrracts 1 from the current value   a=a-1 is the same as a--
            += Addition Assignment Operator a+=4   is the same as a = a + 4
            -= Subtraction Assignment Operator a-=4 is the same as a = a - 4 
            /= Division Assignment Operator a/=4  is the same as a = a / 4
            *= Multiplication Assignment Operator a*=4 is the same as a = a * 4
            
            
            */

            int toChange = 5;   //Value is 5
            toChange++;         //Value is 6  toChange = toChange+1; 
            toChange--;         //Value is 5  toChange = toChange-1;
            toChange += 3;      //Value is 8  toChange = toChange+3;
            toChange -= 2;      //Value is 6  toChange = toChagnge-2;
            toChange /= 2;      //Value is 3  toChange = toChange/2;
            toChange *= 5;      //Value is 15 toChange = toChange*5;
            Console.WriteLine(toChange);



        }
    }
}
