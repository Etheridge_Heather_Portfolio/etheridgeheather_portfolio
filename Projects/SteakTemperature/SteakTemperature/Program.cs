﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteakTemperature
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Heather Etheridge
            November 5, 2016
            Section #02
            */

            //Determine what cooking temperature our steak is for a certain temperature

            /*
            Rare is 130-140
            Medium Rare is 140-145
            Medium 145-160
            Well Done 160-170
            */

            //Ask the user for the temperature of their steak

            Console.WriteLine("We are going to determine the steak doneness level. \r\nWhat is the temperature of your steak in degree F?\r\nPlease press return when done." );

            //Capture the user response and store in in a string variable
            string steakTempString = Console.ReadLine();
            //Convert to a number datatype
            int steakTemp = int.Parse(steakTempString);

            //Start our conditional
            //Test the temp against doneness level
            if (steakTemp < 69) {
                Console.WriteLine("You have a {0}% which means");
            } else if (steakTemp < 140) {
                Console.WriteLine("This steak is rare.");
            } else if (steakTemp < 145) {
                Console.WriteLine("This steak is medium rare.");
            } else if (steakTemp < 160) {
                Console.WriteLine("This steak is medium.");
            } else if (steakTemp<170) {
                Console.WriteLine("This steak is well done.");
            } else {
                Console.WriteLine("The steak is burnt.  Feed it to the dog!");
            }        
        
        }
    }
}
