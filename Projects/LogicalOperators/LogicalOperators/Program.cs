﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicalOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            //Logical Operators

            //And Operator &&

            //Can we buy an iPhone?
            decimal budget = 600m;
            decimal iphoneCost = 500m;
            decimal payCheck = 500m;

            //We can buy the phone IF the phone is less than budget AND our paycheck is greater than 700 to cover our bills

            if (iphoneCost < budget && payCheck > 700)
            {
                //We can buy the phone
                Console.WriteLine("You can purchase the iPhone.");
            }
            else Console.WriteLine("Sorry, you cannot buy the phone.");

            //Or Operator ||

            if (iphoneCost < budget || payCheck > 700)
            {
                //We can buy the phone
                Console.WriteLine("You can purchase the iPhone in the OR case.");
            }
            else Console.WriteLine("Sorry, you cannot buy the phone in the OR case.");

            //Not Operator !
            //Flips a value, turns true to false, and false to true
            // a<b  is the same as  !(a>b)
            // a!=b  is the same as  !(a==b)

            if (!(iphoneCost > budget))
            {
                Console.WriteLine("From the not operator you can buy the phone!");
            }
            else {
                Console.WriteLine("From the not operator you cannot buy the phone!");
            }





        }
    }
}
