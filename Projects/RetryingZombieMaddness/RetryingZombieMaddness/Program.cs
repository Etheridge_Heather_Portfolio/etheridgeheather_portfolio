﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetryingZombieMaddness
{
    class Program
    {
        static void Main(string[] args)
        {
            //Program to find out how many zombies will be created over a set number of days durring the appocolypse
            //How many zombies will there be in 8 days
            //One zombie can bite 4 people a day, the next day those bitten become zombies

            //Givens first
            //Start with one zombie
            //Bites 4 people a day
            //8 Days

            //Turn into variables
            //How many zombies do we have

            int numZombies = 1;

            //number of bites per zombie per day

            int numBites = 4;

            //How many days
            int days = 8;

            //Creat a loop to cycle though each day

            for (int i = 1; i<=days; i++ ) {
                //What happens in a day?
                //How many new zombies get created
                //4*number of zombies
                //since each zombie will bite 4 people
                //need a new variable to hold this
                int newZombies = numZombies * numBites;

                //At the end of the day these new zombies join our zombie horde
                //have to update the number of zombies we have
                numZombies += newZombies;

                //Tell the public how many zombies we have each day
                Console.WriteLine("There are {0} zombies on day number {1}!",numZombies,i);
            }


            //Asking a new question
            //How many days will it take to get to 1 million zombies

            int numDays = 1;
            int zombieHordeNumber = 1;

            while (zombieHordeNumber <= 1000000) {
                //Happens each day
                int bittenPeople = zombieHordeNumber * numBites;

                //At the end of the day the people who were bitten become zombies
                zombieHordeNumber += bittenPeople;
                //Report to the people how many zombies there are
                Console.WriteLine("On day number {0} there are {1} zombies!",numDays,zombieHordeNumber);

                //At the end of the day we increase the day number
                numDays++;
            }

            //So we used the for loop while we knew how many days we needed to cyle thorugh
            //And used the while loop when we didn't know how many times we were going to cycle through

          
        }
    }
}
