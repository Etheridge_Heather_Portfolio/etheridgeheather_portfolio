﻿public static void Main(string[] args)
{
    int lastScore;
    int[] examScores = { 100, 98, 90, 100, 88 };
    Console.WriteLine("Enter your final score: ");
    lastScore = (Console.ReadLine());
    addScore(lastScore, examScores);
}
public static void addScore(int _grade, int[] _gradeArray)
{
    int totalOfScores = 0;
    int numExams = _gradeArray.Length;
    double finalGrade;
    Console.WriteLine(“You have taken “ +numExams + “ exams.”);
    Console.WriteLine(“We’ll compute an average for you that will”) ;
    Console.WriteLine(“include your last grade of ” +_grade + “.”);
    for (int i = 0; i < numExams; i++)
    {
        totalOfScores = totalOfScores + _gradeArray[i];
    }
    totalOfScores += _grade;
    finalGrade = totalOfScores / (_gradeArray.Length + 1);
    finalGrade = Math.Round(finalGrade);
    Console.WriteLine(“Your average for the exams is “ +finalGrade + “.”) ;
}

