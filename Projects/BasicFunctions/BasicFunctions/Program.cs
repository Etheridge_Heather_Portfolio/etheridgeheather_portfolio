﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicFunctions
{
    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Before the function call.");


            //Create a function call that will start our PrintMore() function
            //Put the name of the function and ();
            PrintMore();

            Console.WriteLine("After the function call.");

            //A second function call
            PrintMore();

            //A functino call to the PrintEnjoy() function
            Console.WriteLine("Before enjoy function call");
            PrintEnjoy();

            
        }

        public static void PrintEnjoy()
        {
            Console.WriteLine("I hope that you enjoy this program.");

        }



        public static void PrintMore()
        {
            //This is the code that will run when I "call" this method/function
            string userName = "Heather";
            Console.WriteLine("Welcome " + userName + ", to the program!");

        }





    }
}
