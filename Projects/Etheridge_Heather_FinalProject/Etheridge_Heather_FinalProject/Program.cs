﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etheridge_Heather_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Heather Etheridge
            January 25, 2017
            Section 02
            Final Project
            */

            //Ask the user for a comma seperated list of items they would like to purchase
            Console.WriteLine("Please enter a comma seperated list of items you would like to purchase.");
            string userString = Console.ReadLine();
            //Verify it isn't left blank
            while (string.IsNullOrWhiteSpace(userString)) {
                Console.WriteLine("Please do not leave blank.  Enter a comma seperated list of items you would like to purchase.");
                userString = Console.ReadLine();
            }
            //Get an aray of the items by calling TextToArray
            string[] shoppingList = TextToArray(userString);

            //Get item cost array by calling PromptforCosts
            decimal[] priceList = PromptForCosts(shoppingList);

 
            //Get sum of costs by calling method
            decimal sumOfPrices = SumOfCosts(priceList);
            Console.WriteLine("The total cost of all of your items is {0}.",sumOfPrices.ToString("C"));

        }

        //Create a custom function to split the string into an array
        public static string[] TextToArray(string userString)
        {
            string list = userString;
            string[] listArray = userString.Split(',');
            return listArray;
        }

        //Create a custom function that will take in the array of items and create an array to hold the cost of the items
        public static decimal[] PromptForCosts(string[] userList) {
            string[] userArray = userList;
            decimal[] itemCost = new decimal[userArray.Length];
            for (int i = 0, j = 0; i < userArray.Length; i++, j++) {
                //Prompt the users for item prices
                Console.WriteLine("How much does the following item cost? {0} ", userArray[i].Trim());
                string itemCostString = Console.ReadLine();
                //Validate user input is a decimal
                decimal itemCostDecimal;
                while (!decimal.TryParse(itemCostString, out itemCostDecimal)) {
                    Console.WriteLine("Please only enter numbers and do not leave blank.\r\nHow much does the following item cost? {0}", userArray[i]);
                    itemCostString = Console.ReadLine();
                }
                itemCost[j] = itemCostDecimal;
            }

            return itemCost;
        }

        //Create a custom function that will find the sum of the price of the items
        public static decimal SumOfCosts(decimal[] costArray) {
            decimal[] priceArray = costArray;
            decimal priceTotal=0;

            for(int i=0; i < costArray.Length; i++) {
                priceTotal += priceArray[i];        
            }
            return priceTotal; 
        }
        /*
        Test Values 1       
        Please enter a comma seperated list of items you would like to purchase.
        chair, cup, headphones, pillow
        How much does the following item cost? chair
        12.50
        How much does the following item cost? cup
        2.75
        How much does the following item cost? headphones
        13.00
        How much does the following item cost? pillow
        10.00
        The total cost of all of your items is $38.25.


        TestValues 2
        Please enter a comma seperated list of items you would like to purchase.
        hat, scarf, shoes
        How much does the following item cost? hat
        15
        How much does the following item cost? scarf
        no
        Please only enter numbers and do not leave blank.
        How much does the following item cost?  scarf

        Please only enter numbers and do not leave blank.
        How much does the following item cost?  scarf
        10
        How much does the following item cost? shoes
        15.99
        The total cost of all of your items is $40.99.

        TestValues 3 
        Please enter a comma seperated list of items you would like to purchase.
        milk,   bread  , cheese, water, olive oil, lettuce, meat
        How much does the following item cost? milk
        2.50
        How much does the following item cost? bread
        2.75
        How much does the following item cost? cheese
        3
        How much does the following item cost? water
        4
        How much does the following item cost? olive oil
        3
        How much does the following item cost? lettuce
        2.99
        How much does the following item cost? meat 
        7.25
        The total cost of all of your items is $25.49.      
      */
    }

}

