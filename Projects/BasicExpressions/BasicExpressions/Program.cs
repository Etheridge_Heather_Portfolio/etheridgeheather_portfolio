﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            //Basic Expressions
            //Sequential Programming
            //Line by Line

            //Declare and define a variable
            int a = 2;

            //Change the value of a permanantly

            a = a + 3;
            Console.WriteLine(a);

            //Create a new variable and base it on a different variable
            int b = a + 3;
            Console.WriteLine(b);
            Console.WriteLine(a);

            //Expression that finds our age

            //We need current year
            int currentYear = 2016;

            //Year you were born
            int yearBorn = 1978;

            //Simple subtraction
            //Create a variable to hold the age
            //If you have NOT had your birthday this year, subtract one
            int age = currentYear - yearBorn;
            Console.WriteLine("My current age is " + age);



        }
    }
}
