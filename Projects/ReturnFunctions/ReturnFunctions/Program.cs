﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReturnFunctions
{
    class Program
    {
        static void Main(string[] args)
        {

            // Create tha variables for width and height
            int width = 10;
            int height = 5;

            //Function call
            //Create a variable to "catch" the returned value
            //Whatever is inside () will be passed into CalcArea's() 
            //The first int will become int w and the second will become int h
            int results = CalcArea(width, height); 
            Console.WriteLine(results);
            //You don't even have to create variables, you can pass in numbers
            int results2 = CalcArea(3,4);
            Console.WriteLine(results2);
          

        }

        public static int CalcArea(int w, int h) {
            //Inside of function CalcArea
           

            //Calculate the area of a rectangle
            // int area = width * height;
            int area = w * h;

            //Return the area
            return area;
            
            }


    }
}
