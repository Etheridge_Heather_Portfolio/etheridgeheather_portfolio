﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetryingReturnFunctions
{
    class Program
    {
        static void Main(string[] args)
        {

            //Create the variables for width and height
            int width1 = 10;
            int height1 = 5;


            //Function Call
           int results = CalcArea(width1, height1);
            //Need to create a variable to "Catch" the returned value

            //Then you can use the returned value
            Console.WriteLine(results);

            //You can also put in your own values
            int results2 = CalcArea(3,4);
            Console.WriteLine(results2);
            
        }

        public static int CalcArea(int w, int h) {
            //Inide of function CalcArea

            //Create two variables for width and length
            //int width = 6;
            //int height = 7;

            //Calculate the area of a rectangle
            //int area = width * height;

            int area = w * h;

            //Because you included a return type of int the Function will not even run 
            //If you do not return an int

            //Return the area 
            return area;
        }

    }
}
