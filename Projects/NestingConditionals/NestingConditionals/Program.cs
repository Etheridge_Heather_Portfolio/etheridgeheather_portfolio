﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestingConditionals
{
    class Program
    {
        static void Main(string[] args)
        {

            //Nesting Conditionals
            //Some decicions affect other decisions

            //If it is warm enough let's go to the beach
            //If not, let's see a movie
            //If the water is warm then let's go swimming, If not, then let's get a tan

            //Create a temperature variable
            int temp = 80;

            //Create a water temperature variabale
            int waterTemp = 85

            //Test to see if we can go to the beach
            if (temp >= 85)
            {
                Console.WriteLine("It is warm enough, let's go to the beach!");
                //Nest conditional here
                if (waterTemp >= 75)
                {
                    Console.WriteLine("Let's go swimming!");
                }
                else {
                    Console.WriteLine("Water is a little cold, let's go get a tan!");
                }
                           }
            else {
                Console.WriteLine("It is cold, let's go to the movies!");
            }





        }
    }
}
