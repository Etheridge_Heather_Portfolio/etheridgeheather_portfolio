﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            //While Loop

            //Declare and define a starting counting variable
            int counter = 0;

            while (counter<5) {
                Console.WriteLine("The counter is {0}", counter);
                //Increment of change to counter variable
                counter++;

            
           

            }

            //Just for fun :) 100 bottles of beer on the wall
            int bottlesOfBeer = 10;
            while (bottlesOfBeer>1) {
              Console.WriteLine("{0} Bottles of beer on the wall {0} Bottles of beer\r\nYou take one down, pass it around {1} Bottles of beer on the wall!" , bottlesOfBeer, --bottlesOfBeer);


            }


            Console.WriteLine("Please enter your name and then hit return.");
            //Save users response
            string userName = Console.ReadLine();
 
            //Validate that the user did not leave it blank
            //IsNullOrWhitSpace for text strings
            while (string.IsNullOrWhiteSpace(userName)) {

                //Tell the user what's happening
                Console.WriteLine("PLease do not leave blank.\r\nPlease type in your name.");
                //Re-define the variable
                userName = Console.ReadLine();

            }

            //Convert to number and validate
            Console.WriteLine("Hi {0}! Please enter a number.", userName);

            //Save responses
            string numberString = Console.ReadLine();
            //Declare a variable to hold the converted value
            double userNum;
            //While loop validation for number
            while (!double.TryParse(numberString, out userNum)) {
                //Tell the user the problem
                Console.WriteLine("You have typed something other than a number.\r\nPlease enter a number:");
                //Recatch the user response using same variable above
                numberString = Console.ReadLine();
         
            }

            //Test for us
            Console.WriteLine("The number you typed in was {0}\r\n" , userNum);



            //Do While Loop
            //Declare and define the variable to test
            // i - iteration -
            int i = 0;
            //Do while loops always run at least once and then they check
            do {
                Console.WriteLine("The value of i is {0}", i);
                //Increment of change
                i++;

            } while (i<5);


        }
    }
}
