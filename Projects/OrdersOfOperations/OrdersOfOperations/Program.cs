﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdersOfOperations
{
    class Program
    {
        static void Main(string[] args)
        {
            //Order of Operations
            //PEMDAS - Please Excuse My Dear Aunt Sally
            //Parenthisis, Exponents, Multiply, Divide, Addition, Subraction

            //Find the average of 4 quizes

            //Create variables for the quiz values
            double quiz1 = 85;
            double quiz2 = 100;
            double quiz3 = 80;
            double quiz4 = 90;

            //Average is add up everything and then divide by the number of items
            double average = (quiz1 + quiz2 + quiz3 + quiz4) / 4;

            //Add descriptive text
            Console.WriteLine("The average of the quizes is " + average);

            //Too many parenthisis is not a good thing
            //Find the perimeter of a rectangle
            // 2*length + 2*width

            int lengthRect = 8;
            int widthRect = 6;
            int perimeterRect = 2 * lengthRect + 2 * widthRect;
            Console.WriteLine(perimeterRect);


        }
    }
}
