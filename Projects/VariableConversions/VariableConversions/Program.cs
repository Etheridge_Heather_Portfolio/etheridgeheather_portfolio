﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariableConversions
{
    class Program
    {
        static void Main(string[] args)
        {

            //Implicit conversions
            //smaller data type to a larger one
            short num = 23456;
            Console.WriteLine(num);

            //implicity convert
            int bigNum = num;
            Console.WriteLine(bigNum);

            //Explicit conversions
            //Be careful and watch for data loss!!!
            double x = 1234.56;
            Console.WriteLine(x);

            //Convert to int
            //cuts of everything it cant handle
            int xConverted = (int)x;
            Console.WriteLine(xConverted);

            //Convert a large number to an s byte
            int z = 130;
            sbyte zConverted = (sbyte)z;
            Console.WriteLine(z);
            Console.WriteLine(zConverted);

            //Helper class conversions

            //Convert class
            string stringValue = "56";

            //try to multiply times 2
            //int multiplied = stringValue * 2;
            // does not work

            //Convert string to a number and then multiply

            int multiplied = Convert.ToInt32(stringValue);
            Console.WriteLine(stringValue);
            //multiplied = multiplied * 2;
            //or
            multiplied *= 2;
            Console.WriteLine(multiplied);

            //Parse
            // Converts a string version into a different data type
            //Good for prompting the user!

            //Create a string variable
            string salary = "150000";

            //Parse the string and pull out an integer
            int salaryInt = int.Parse(salary);

            //Divide salary by 4 for quarterly total
            salaryInt = salaryInt / 4;

            Console.WriteLine(salaryInt);



        
       



        }
    }
}
