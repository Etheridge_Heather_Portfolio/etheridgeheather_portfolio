﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicBookProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create an instancce of our comic book class
            //Creates an object with all of the information attatched to it
            //Don't forget that in parameters decimals need that m after them!!
            ComicBook avengers = new ComicBook("Avengers Annual #16",1987,1.25m,4.00m);

            //Console the name of the book
            Console.WriteLine("The title of the first comic book I ever bought is "+avengers.GetTitle());
            Console.WriteLine("The current value of my first comic is "+avengers.GetCurrentValue());

            //Lets change the current value of the comic book by using a setter
            avengers.SetCurrentValue(20.00m);
            Console.WriteLine("The current value of my comic book after the movie came out is "+avengers.GetCurrentValue());

            //Create a second comic book
            ComicBook spiderman = new ComicBook("The Amazing Spider Man #194",1979,.40m,2000.00m);
            Console.WriteLine("The first apearance of Black Cat is in "+spiderman.GetTitle());
            //Find the new worth of the spiderman comic book
            Console.WriteLine("The current net worth of the spiderman comic is "+spiderman.NetWorth());

            //Oh no!! some one spilled coffee on the spider man comic
            //Lets set a new price :(
            spiderman.SetCurrentValue(100.00m);
            Console.WriteLine("The current net worth of the spiderman comic after the damage is " + spiderman.NetWorth());

            Console.WriteLine("The Avengers comic book is {0} years old.", avengers.Age(2017));
            Console.WriteLine("The SpiderMan comic book is {0} years old.", spiderman.Age(2017));

        }
    }
}
