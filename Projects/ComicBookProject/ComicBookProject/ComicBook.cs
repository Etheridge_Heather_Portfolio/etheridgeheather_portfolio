﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicBookProject
{
    class ComicBook
    {
        //Create the member variables
        //These describe the comic book
        //lowercase m signifies that it is a member variable
        string mTitle;
        int mYearPublished;
        decimal mOriginalCost;
        decimal mCurrentValue;

        //Create our constructor function
        //Make sure that the constructor function has the same name as the class
        //So that once it is istantiated it will use this as the constructor function
        public ComicBook(string _title, int _yearPublished, decimal _originalCost, decimal _currentValue) {
            //Use the incoming parameters to initialize our original member variables
            mTitle = _title;
            mYearPublished = _yearPublished;
            mOriginalCost = _originalCost;
            mCurrentValue = _currentValue;
        }

        //Next you need to set up your getters and setters

        //Getters - returns the information back to where it was called
        public string GetTitle() {
            //Return the value of the title
            return mTitle;
        }
        public int GetYearPublished() {
            return mYearPublished;
        }
        public decimal GetOriginalCost() {
            return mOriginalCost;
        }
        public decimal GetCurrentValue() {
            return mCurrentValue;
        }

        //Setters change the member variables
        public void SetTitle(string _title) {
            //Change the member ariable and use the parameter
            mTitle = _title;
        }
        public void SetYearPublished(int _yearpublished) {
            mYearPublished = _yearpublished;
        }
        public void SetOriginalCost(decimal _orignialCost) {
            mOriginalCost = _orignialCost;
        }
        public void SetCurrentValue(decimal _currentValue) {
            mCurrentValue = _currentValue;
        }

        //Create a custom method inside of our class
        //Calculate our net worth 
        //Current value and subtract original cost
        //Return it as a formated text string
        public string NetWorth() {
            decimal netWorth = mCurrentValue - mOriginalCost;
            //Convert the decimal into a formated text string
            string netWorthFormatted = netWorth.ToString("C");
            return netWorthFormatted;
        }

        //Let's create a method that tells us how old the comic book is
        //Take in an argument of the current year
        public int Age(int _currentYear) {
            int age = _currentYear - mYearPublished;
            return age;
        }



    }
}
