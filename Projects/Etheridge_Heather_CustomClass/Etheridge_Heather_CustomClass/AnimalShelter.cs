﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etheridge_Heather_CustomClass
{
    class AnimalShelter
    {
        //The member variables
        string mShelterName;
        int mHighestNumber;
        int mLowestNumber=0;
        int mCurrentNumber;
        int mAddDog;

        //Constructor Function
        public AnimalShelter(string _shelterName, int _highestNumber, int _currentNumber) {
            mShelterName = _shelterName;
            mHighestNumber = _highestNumber;
            mCurrentNumber = _currentNumber;
        }

        //Getters
        public string GetShelterName() {
            return mShelterName;
        }
        public int GetHighestNumber() {
            return mHighestNumber;
        }
        public int GetLowestNumber() {
            return mLowestNumber;
        }
        public int GetCurrentNumber() {
            return mCurrentNumber;
        }
        public int GetAddDog() {
            return mAddDog;
        }

        //Setters
        private void SetShelterName(string _shelterName) {
            mShelterName = _shelterName;
        }
        private void SetHighestNumber(int _highestNumber) {
            mHighestNumber = _highestNumber;
        }        
        private void SetCurrentNumber(int _currentNumber) {
            if (_currentNumber > mLowestNumber && _currentNumber <= mHighestNumber)
            {
                mCurrentNumber = _currentNumber;
            }
            else {
                Console.WriteLine("The current number must be more than zero and lower than the highest number of dogs the shelter can have.\r\nPlease try again.");            }
        }
        private void SetLowestNumber(int _lowestNumber) {
            if (_lowestNumber > 0)
            {
                mLowestNumber = _lowestNumber;
            }
            else {
                Console.WriteLine("The Lowest number cannot be less than 0. Please Try Again.");
            }
        }
        public void SetAddDog(int _addDog) {
            if (_addDog + mCurrentNumber <= mHighestNumber)
            {
                mAddDog = _addDog;
            }
            else {
                Console.WriteLine("The total number of dogs can not exceed the highest number allowed.\r\nPlease try again.");
            }
        }

        //Custom methods that allows us to change the number of dogs in the shelter
        public int AddDog () {
            mCurrentNumber = mCurrentNumber + mAddDog;
            return mCurrentNumber;
        }

        public void RemoveDog(int _dogsRemoved)
        {
            if (mCurrentNumber - _dogsRemoved >= 0) {
                mCurrentNumber = mCurrentNumber - _dogsRemoved;
            } else {
                Console.WriteLine("The total number of dogs cannot go below 0.\r\n Please try again");
            }
        }


            

       
        

    }
}
