﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtheridgeHeather_CodingChallenges
{
    class Program
    {
        static void Main(string[] args)
        {

          
            int age;

     
            string userChoice = Menu();
            //Use input from user to select menu option

            if (userChoice == "1")
            {
                Console.WriteLine("You have chosen the program Swap Name. We will swap your first and last name.");
                Console.WriteLine("Please enter your first name.");
                string firstName = Console.ReadLine();
                Console.WriteLine("Got it! Now, please enter your last name.");
                string lastName = Console.ReadLine();
                Console.WriteLine("Your original name was {0} {1}, and your new name is {2}", firstName, lastName, SwapName(firstName, lastName));
                Console.WriteLine("\r\n");
                
            }
            else if (userChoice == "2")
            {
                Console.WriteLine("You have chosen the program Backwards. We will reverse the words in a sentence you enter.");
                Console.WriteLine("Please enter a sentence with at least 6 words.");
                string sentence = Console.ReadLine();
                //Turn the users scentence into an array that can be rearranged in the method.
                string[] sentenceArray = sentence.Split(' ');
                //Return reversed sentence to user.
                Console.WriteLine("The original sentenced you entered was:\r\n{0}", sentence);
                Console.WriteLine("Your sentence reversed is:\r\n{0}", Backwards(sentenceArray));
                Console.WriteLine("\r\n");
                
            }
            else if (userChoice == "3")
            {
                //Ask for users name
                Console.WriteLine("You have chosen the program Age Converter. Please enter your name");
                string userName = Console.ReadLine();
                //Validate user did not leave blank
                while (string.IsNullOrEmpty(userName))
                {
                    Console.WriteLine("You did not enter anything.  Please do not leave blank.");
                    Console.WriteLine("What is your name?");
                    userName = Console.ReadLine();
                }
                //Ask for uers age
                Console.WriteLine("Please enter your age.");
                string ageString = Console.ReadLine();
                //Validate user entered an integer           
                while (!int.TryParse(ageString, out age))
                {
                    Console.WriteLine("You have entered something other than a number.  Please enter your age using numbers.");
                    ageString = Console.ReadLine();
                }
                AgeConvert(userName, age);
               
                
            }
            else {
                Console.WriteLine("You have chosen the program Temperature Converter. We can convert temperatures between Farenheit and Celcius.\r\nPlease enter a temperature in Farenheit.");
                string stringTempF = Console.ReadLine();
                //Validate that user entered a number
                decimal tempF;
                while (!decimal.TryParse(stringTempF, out tempF)) {
                    Console.WriteLine("You have entered something other than a number.\r\nPlease enter a temperature in Farenheit.");
                    stringTempF = Console.ReadLine();
                }
                TempConvert(tempF, 1);
                Console.WriteLine("Please enter a temperature in Celcius.");
                string stringTempC = Console.ReadLine();
                decimal tempC;
                while (!decimal.TryParse(stringTempC, out tempC)) {
                    Console.WriteLine("You have entered something other than a number.\r\nPlease enter a temperature in Celcius.");
                    stringTempC = Console.ReadLine();
                }
                TempConvert(tempC,2);
               
            }

            

            }

        //SwapName method

        public static string SwapName(string firstName, string lastName)
        {   
            //Swaps first and last name imput by user.
            string newFirst = lastName;
            string newLast = firstName;
            string newName = newFirst + " " + newLast;

            //Returns new name
            return newName;
        }

        //Backwards method

        public static string Backwards(string[] sentenceArray)
        {
            string[] reversedArray = new string[sentenceArray.Length];

            int j = 0;
            for (int i =sentenceArray.Length - 1; i >= 0; i--)
            {
                reversedArray[j] = sentenceArray[i];
                j++;
            }

            string newSentence = String.Join(" ",reversedArray);
            return newSentence;
        }

        //Age Converter method

        public static void AgeConvert(string name, int age)
        {
            int daysAlive = 365 * age;
            int hoursAlive = daysAlive * 24;
            int minutesAlive = hoursAlive * 60;
            int secondsAlive = minutesAlive * 60;

            Console.WriteLine("Hello {0}! You are {1} years old, which is the same as:",name,age);
            Console.WriteLine("{0} days or\r\n{1} hours or \r\n{2} minutes or \r\n{3} seconds.",daysAlive,hoursAlive,minutesAlive,secondsAlive);
        }

        //Temperature Converter method

        public static void TempConvert(decimal temp, int type)
        {
            if (type == 1)
            {
                decimal tempC = (temp - 32m)*(5m/9m);
                Console.WriteLine("You entered a temperature of {0} degrees Farenheit, that converts to {1} degrees Celcius.", temp,Math.Round(tempC,2));
            }
            else {
                decimal tempF = (temp * (9m / 5m)) + 32m;
                Console.WriteLine("You entered a temperature of {0} degrees Celcius, that convers to {1} degrees Farenheit.",temp,Math.Round(tempF,2));
            }
        }

        //Menu method

        public static string Menu() {
            //Create User menu
            Console.WriteLine("Hello! Choose a program below by entering its corresponding number.");
            Console.WriteLine("1.Swap Name\r\n2.Backwards\r\n3.Age Converter\r\n4.Temperature Converter");
            //Capture User response
            string userChoice = Console.ReadLine();
            //Validate user response
            while (userChoice != "1" && userChoice != "2" && userChoice != "3" && userChoice != "4")
            {
                //Alert user to the issue
                Console.WriteLine("You have entered something other than a number 1-4.");
                Console.WriteLine("Please choose a program below by entering its corresponding number.");
                Console.WriteLine("1.Swap Name\r\n2.Backwards\r\n3.Age Converter\r\n4.Temperature Converter");
                userChoice = Console.ReadLine();
            }
            return userChoice;
        }




    }
}
