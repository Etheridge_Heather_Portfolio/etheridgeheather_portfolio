﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidatingUserPrompts
{
    class Program
    {
        static void Main(string[] args)
        {

            //Validating user prompts with conditionals

            //Ask the user for their name
            Console.WriteLine("Hello, please enter your name, and then hit enter.");

            //Store results in a string variable
            string userName = Console.ReadLine();

            //Test to see if the user left it blank
            //string.IsNullOrWhiteSpace(where variable to check will go)
            //Returns a boolean of true if it is blank
            //Returns a boolean value of false if it contains anything

            //Console.WriteLine(string.IsNullOrWhiteSpace(userName)); 

            //Create a conditional to check if the user inputted anything
            if (string.IsNullOrWhiteSpace(userName)) {
                //if this runs it means the user left it blank
                //tell the user what went wront
                Console.WriteLine("Please do no leave this blank, enter your name.");

                //Capture the response again
                userName = Console.ReadLine();
            }

            Console.WriteLine("Hello " + userName + "! Thanks for running my program!");

            Console.WriteLine("Please enter an integer between 1 and 10");
            string userIntString = Console.ReadLine();

            //Convert the string to an int
            //Instead of parse, use try parse!!
            //TryParse int.TryParse(variable to try to convert, out variable name to hold the converted number)
            //TryParse returns a boolean value of true if the conversion works
            //returns a boolean of false if there is an error.
            //It will not throw a runtime error for our program!!!

            //Declare a variable to hold the converted output value

            int userInt;

            //Create a conditional to test validation

            if (!(int.TryParse(userIntString, out userInt))) {
                //This will run if it could not convert it
                //Tell the user the problem with a Console.WriteLine
                Console.WriteLine("Oops, you typed in something other than an integer between 1 and 10\r\nPlease enter an integer 1-10:");

                //Re-capture the user response - use the same string as above
                userIntString = Console.ReadLine();

                //Have to re convert it to int
                int.TryParse(userIntString, out userInt);
            }


            Console.WriteLine(int.TryParse(userIntString, out userInt));

            Console.WriteLine(userInt*2);



















        }
    }
}
