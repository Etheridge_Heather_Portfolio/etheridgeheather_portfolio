﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            //Math Operators
            // + Addition (or Concatonation when dealing with text)
            // - Subtraction
            // * Multiplication
            // / Division

            //Area of a triangle
            //(Width * Height) / 2 

            int width = 4;
            int height = 5;
            int areaTriangle = width * height / 2;
            Console.WriteLine(areaTriangle);

            //Add descriptive text for our final users
            Console.WriteLine("The area of a triangle with a width of " + width + " and a height of " + height + " is " + areaTriangle);

            //Modulo % 
            //Gives the remainder left over
            decimal normalDivision = 36 / 10;
            Console.WriteLine(normalDivision);

            int remainderVal = 15 % 7;
            Console.WriteLine(remainderVal);

            //Tell if something is an even or odd
            // %2 - if 1 then it is odd if 0 it is even

            int evenOrOdd = 31 % 2;
            Console.WriteLine(evenOrOdd);


        }
    }
}
